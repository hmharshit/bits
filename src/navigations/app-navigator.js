import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../scenes/home';
import JoinScreen from '../scenes/join';
import CallScreen from '../scenes/call';
import SettingScreen from '../scenes/setting';
import ProfileScreen from '../scenes/profile';
import HostingScreen from '../scenes/hosting';
import PlanScreen from '../scenes/plan';
import PaymentScreen from '../scenes/payment';
import ScheduleScreen from '../scenes/schedule';
import VideoCall from "../scenes/conference"
import ProfileEditScreen from '../scenes/profileEdit';
import ChatScreen from '../scenes/chat';
import EditSchdeduleScreen from '../scenes/EditSchedule'


const AppNavigatorConfig = {
  initialRouteName: 'Home',
  headerMode: 'none',
  header: null
};

const RouteConfigs = {
  Home: HomeScreen,
  Join: JoinScreen,
  Call: CallScreen,
  Setting: SettingScreen,
  Profile: ProfileScreen,
  ProfileEdit: ProfileEditScreen,
  Hosting: HostingScreen,
  Plan: PlanScreen,
  Payment: PaymentScreen,
  Schedule: ScheduleScreen,
  EditSchedule: EditSchdeduleScreen,
  VideoCall: VideoCall,
  Chat: ChatScreen
};


const AppNavigator = createStackNavigator(RouteConfigs, AppNavigatorConfig);


export default AppNavigator;
