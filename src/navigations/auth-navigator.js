import { createStackNavigator } from 'react-navigation-stack';

import LandingScreen from '../scenes/landing';
import LoginScreen from '../scenes/login';
import SignupScreen from '../scenes/signup';
import PasswordScreen from '../scenes/password';
import ForgotPasswordScreen from '../scenes/forgotpassword';
import NewPasswordScreen from "../scenes/newpassword"
import JoinScreen from '../scenes/join';

const AuthNavigatorConfig = {
  initialRouteName: 'Landing',
  header: null,
  headerMode: 'none',
};

const RouteConfigs = {
  Landing: LandingScreen,
  Login: LoginScreen,
  Join: JoinScreen,
  SignUp: SignupScreen,
  Password: PasswordScreen,
  ForgotPassword: ForgotPasswordScreen,
  NewPassword: NewPasswordScreen
};

const AuthNavigator = createStackNavigator(RouteConfigs, AuthNavigatorConfig);

export default AuthNavigator;
