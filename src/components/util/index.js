import React from 'react';
import { Root, Container, Text, Header, Left, Body, Right, Title, Content, Button, Toast } from 'native-base';
import {Colors} from '../../styles'

export const Notification = (message, type) => {

    var customType = 'warning'
    var alertColor = Colors.WARNING

    if (type === 'success') {
        customType = 'succes'
        alertColor = Colors.PRIMARY
    }else if (type === 'error'){
        customType = 'danger'
        alertColor = Colors.RED
    }

    Toast.show({
        text: message,
        type:customType,
        position:'bottom',
        textStyle: {
            textAlign: 'center'
          },
          style: {
            backgroundColor: alertColor

           }
          
        
    })



    // if (type === 'success') {
    //     Toast.show(message, {
    //         containerStyle: {
    //             backgroundColor: 'green',
    //             paddingHorizontal: 15,
    //             borderRadius: 20
    //         },
    //         textStyle: {
    //             fontSize: 15,
    //             fontFamily: 'Prompt-Regular'
    //         },
    //         textColor: '#fff',
    //         duration: 1000
    //     })
    // } else if (type === 'error') {
    //     Toast.show(message, {
    //         containerStyle: {
    //             backgroundColor: 'red',
    //             paddingHorizontal: 15,
    //             borderRadius: 20
    //         },
    //         textStyle: {
    //             fontSize: 15,
    //             fontFamily: 'Prompt-Regular'
    //         },
    //         textColor: '#fff',
    //         duration: 1000
    //     })
    // } else if (type === 'info') {
    //     Toast.show(message, {
    //         containerStyle: {
    //             backgroundColor: 'purple',
    //             paddingHorizontal: 15,
    //             borderRadius: 20
    //         },
    //         textStyle: {
    //             fontSize: 15,
    //             fontFamily: 'Prompt-Regular'
    //         },
    //         textColor: '#fff',
    //         duration: 1000
    //     })
    // } else if (type === 'warning') {
    //     Toast.show(message, {
    //         containerStyle: {
    //             backgroundColor: 'orange',
    //             paddingHorizontal: 15,
    //             borderRadius: 20
    //         },
    //         textStyle: {
    //             fontSize: 15,
    //             fontFamily: 'Prompt-Regular'
    //         },
    //         textColor: '#fff',
    //         duration: 1000
    //     })
    // } else {
    //     Toast.show(message, {
    //         textStyle: {
    //             fontSize: 15,
    //             fontFamily: 'Prompt-Regular'
    //         }
    //     })
    // }
};