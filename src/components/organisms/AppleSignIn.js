import React from "react";
import { View, Text, TouchableOpacity, AsyncStorage, Image, Platform } from "react-native";
import { CommonStyles, Colors, Images } from "../../styles";
 import appleAuth, {
     AppleAuthRequestOperation,
     AppleAuthRequestScope,
   } from '@invertase/react-native-apple-authentication';
import { LoginUserApple } from "../../services/auth_services"
import { Notification } from "../util"
import { setIsUserLoggedIn, setUserData } from "../../actions/home"

import {connect} from "react-redux"

class AppleSignIn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      IsLoader: false 
    }
  }
  componentDidMount() {
     appleAuth.onCredentialRevoked(async () => {
         console.warn('If this function executes, User Credentials have been Revoked');
       });
  }

  isSupported = () => {
    const majorVersionIOS = parseInt(Platform.Version, 10)
    return majorVersionIOS >=13 && Platform.OS === "ios" ? true : false
  }


  storeUserDetails = async (user) => {
    console.log("storing user details")
  this.props.dispatch(setIsUserLoggedIn(true))
    this.props.dispatch(setUserData(user))
    await AsyncStorage.setItem("user", JSON.stringify(user))
  }


  _signIn = async () => {

    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGIN,
      requestedScopes: [AppleAuthRequestScope.EMAIL, AppleAuthRequestScope.FULL_NAME],
    });

    console.log("####", appleAuthRequestResponse)
  


    const email = appleAuthRequestResponse.email
    const firstName = appleAuthRequestResponse.fullName.givenName || ""
    const middleName = appleAuthRequestResponse.fullName.middleName || ""
    const lastName = appleAuthRequestResponse.fullName.familyName || ""
    const name = `${firstName} ${middleName} ${lastName}`
    const appleId = appleAuthRequestResponse.user


      LoginUserApple(null, email, name, appleId).then(resp => {

          if(resp && !resp.error) {
            console.log("here")
              resp = resp.result
              if(resp.message && resp.message == "User logged in sucessfully.") {
                console.log("here1")
                console.log("user_authenticated")
                this.storeUserDetails(resp.user)
                Notification("Successfully Logged In!", "success")
                this.props.navigation.navigate("Home")
              }
          }
        })
  };

  render() {
    return (
      this.isSupported() && 
       <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
      <TouchableOpacity style={[CommonStyles.ZButton.fullred, CommonStyles.ZView.centerImageView, { backgroundColor: 'black' }]} onPress={this._signIn}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 0.30, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end' }}>
            <Image source={Images.apple} />
          </View>
          <View style={{ flex: 0.70, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-start' }}>
    <Text style={{ color: 'white', left: 20, fontFamily: 'Prompt-Regular' }}>{this.props.text}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View> 
    );
  }
}


const _mapStateToProps = (state) => {
  return {

  }
}

export default connect(_mapStateToProps)(AppleSignIn)