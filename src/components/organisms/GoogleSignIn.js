import React from "react";
import { View, Text, TouchableOpacity, AsyncStorage, Image, Platform } from "react-native";
import { CommonStyles, Colors, Images } from "../../styles";
import Loader from "../molecules/Loader";

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "@react-native-community/google-signin";
import { LoginUserGmail } from "../../services/auth_services"
import { Notification } from "../util"
import { setIsUserLoggedIn, setUserData } from "../../actions/home"

import {connect} from "react-redux"

class GoogleSignInIcon extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      IsLoader: false 
    }
  }
  componentDidMount() {
    GoogleSignin.configure({
      scopes: [], // what API you want to access on behalf of the user, default is email and profile
      webClientId:
        "1085746971360-d0gsdlu48nbvp7sng2vr984fk0s5r8ba.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.,
      iosClientId: "1085746971360-a68at1nl5vipu76g177or76ad4ave1lq.apps.googleusercontent.com"
    });
  }


  storeUserDetails = async (user) => {
    console.log("storing user details")
  this.props.dispatch(setIsUserLoggedIn(true))
    this.props.dispatch(setUserData(user))
    await AsyncStorage.setItem("user", JSON.stringify(user))
  }


  _signIn = async () => {


    if (Platform.OS === "android") {
      this.setState({ IsLoader: true });
    }


    const isUserSignedIn = await GoogleSignin.isSignedIn(); 

    if (isUserSignedIn) {
      await GoogleSignin.revokeAccess()
    }

    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo)

      let user = userInfo.user

      let user_payload = {
        name: `${user.givenName} ${user.familyName}`.replace(/(^\s+|\s+$)/g,''),
        email: user.email,
        profile_image: user.photo,
        google_id: user.id
      }

      console.log(user_payload)

      LoginUserGmail(user_payload.profile_image, user_payload.email, 
        user_payload.name, user_payload.google_id).then(resp => {

          this.setState({ IsLoader: false });

          if(resp && !resp.error) {
            console.log("here")
              resp = resp.result
              if(resp.message && resp.message == "User logged in sucessfully.") {
                console.log("here1")
                console.log("user_authenticated")
                this.storeUserDetails(resp.user)
                Notification("Successfully Logged In!", "success")
                this.props.navigation.navigate("Home")
              }
          }
        })


      // this.setState({ userInfo });
    } catch (error) {
      console.log(error)
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        await GoogleSignin.revokeAccess()
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  render() {
    return (
       <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
      <TouchableOpacity style={[CommonStyles.ZButton.fullred, CommonStyles.ZView.centerImageView, { backgroundColor: Colors.RED }]} onPress={this._signIn}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 0.30, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end' }}>
            <Image source={Images.google} />
          </View>
          <View style={{ flex: 0.70, flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-start' }}>
    <Text style={{ color: 'white', left: 20, fontFamily: 'Prompt-Regular' }}>{this.props.text}</Text>
          </View>
        </View>
      </TouchableOpacity>
              <Loader 
              setModalVisible={this.state.IsLoader}
              onClose={() => this.setState({IsLoader: false})}
              ></Loader>
    </View> 
    );
  }
}


const _mapStateToProps = (state) => {
  return {

  }
}

export default connect(_mapStateToProps)(GoogleSignInIcon)