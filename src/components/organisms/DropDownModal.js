
import React from 'react';
import PickerModal from 'react-native-picker-modal-view';


export default class DropDownModal extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			selectedItem: {}
		};
	}

	render() {

		return (
				<PickerModal
					renderSelectView={(disabled, selected, showModal) => this.props.render(disabled, selected, showModal)}
					onSelected={this.onSelected.bind(this)}
					onClosed={this.onClosed.bind(this)}
					onBackButtonPressed={this.onBackButtonPressed.bind(this)}
					items={this.props.data}
                    sortingLanguage={'tr'}
                    modalAnimationType={"slide"}
					showToTopButton={true}
					selected={this.props.selected}
					showAlphabeticalIndex={this.props.hideIndex ? false : true}
					autoGenerateAlphabeticalIndex={true}
					selectPlaceholderText={'Choose one...'}
					onEndReached={() => console.log('list ended...')}
					searchPlaceholderText={'Search...'}
					requireSelection={false}
					autoSort={false}
				/>
		);
	}

	onClosed() {
        console.log('close key pressed');
        this.props.onClose()
	}

	onSelected(selected){
		this.props.onSelect(selected)
		return selected;
	}

	onBackButtonPressed() {
        this.props.onBackButton()
		console.log('back key pressed');
	}
}