import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity, StyleSheet, Modal } from 'react-native';
import { FlatList, TextInput, Images, CommonStyles, Colors, Typography } from '../../styles'
import ZingTextInput from '../molecules/ZingTextInput'


export default class MeetingCard extends Component {
    constructor(props) {
        super(props)
    }

    
    render() {
        const { id, title, date } = this.props
        return (
            <View style={{ flex: 1, marginBottom: 10, marginTop:10, height: 90, borderLeftColor: Colors.PRIMARY, borderLeftWidth: 5 }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={{ flex: 1, marginLeft: 10,  justifyContent: 'space-evenly' }} >
                        <Text style={{  fontSize:Typography.FONT_SIZE_16 }}>{title}</Text>
                        <Text style={{  color: 'gray', fontSize:Typography.FONT_SIZE_16 }}>Meeting ID: {id}</Text>
                        <Text style={{  color: Colors.PRIMARY, fontSize:Typography.FONT_SIZE_12 }}>{date}</Text>
                    </View>
                    <View style={{ flex: 1, maxWidth: 120, alignItems: 'center', justifyContent: 'center' }} >
                        <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, { width: 100, maxHeight: 30, borderRadius: 5, justifyContent: 'center', alignItems: 'center' }]}>
                            <Text style={{ color: 'white' }}>Start</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
