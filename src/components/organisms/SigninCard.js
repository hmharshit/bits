import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import ZingTextInput from "../molecules/ZingTextInput";

var phoneOptions = false;
var title = "";
var firstInputPlaceholder = "";
var secondInputPlaceholder = "";
var forgotButtonTitle = "";

class SignInCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      phone: "",
      password: "",
      modalVisible: false,
      cardType: this.props.cardType
    };

    this.forgotPassowrd = this.forgotPassowrd.bind(this);
    this.commonAction = this.commonAction.bind(this);
  }

  componentDidMount() {
    let phoneOptions, firstInputPlaceholder, secondInputPlaceholder, forgotButtonTitle, title

    if (this.state.cardType == "signup") {
      phoneOptions = true;
      firstInputPlaceholder = "Phone number";
      secondInputPlaceholder = "OTP";
      forgotButtonTitle = "Resend OTP";
      title = "For Sign Up, Please fill the details \n below";
    } else if (this.state.cardType == "signin") {
      phoneOptions = true;
      firstInputPlaceholder = "Phone number";
      secondInputPlaceholder = "Password";
      forgotButtonTitle = "Forgot Password ?";
      title = "For Sign In, Please fill the details \n below";
    } else if (this.state.cardType == "signup_password") {
      phoneOptions = false;
      firstInputPlaceholder = "New Password";
      secondInputPlaceholder = "Confirm Password";
      forgotButtonTitle = "Forgot Password ?";
      title = "For Sign In, Please fill the details \n below";
      title = "We have successfully verified your mobile. Please enter the password to signin";
    } else if (this.state.cardType == "forgot_password") {
      phoneOptions = true;
      firstInputPlaceholder = "Phone number";
      secondInputPlaceholder = "OTP";
      forgotButtonTitle = "Resend OTP";
      title = "You can reset your password here";
    }

    this.setState({
      phoneOptions: phoneOptions,
      firstInputPlaceholder: firstInputPlaceholder,
      secondInputPlaceholder: secondInputPlaceholder,
      forgotButtonTitle: forgotButtonTitle,
      title: title,
      cardType: this.props.cardType
    })
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.cardType == "signup") {
      phoneOptions = true;
      firstInputPlaceholder = "Phone number";
      secondInputPlaceholder = "OTP";
      forgotButtonTitle = "Resend OTP";
      title = "Please fill the details \n below";
    } else if (nextProps.cardType == "signin") {
      phoneOptions = true;
      firstInputPlaceholder = "Phone number";
      secondInputPlaceholder = "Password";
      forgotButtonTitle = "Forgot Password ?";
      title = "For Sign In, Please fill the details \n below";
    } else if (nextProps.cardType == "signup_password") {
      phoneOptions = false;
      firstInputPlaceholder = "New Password";
      secondInputPlaceholder = "Confirm Password";
      forgotButtonTitle = "Forgot Password ?";
      title = "For Sign In, Please fill the details \n below";
      title = "We have successfully verified your mobile. Please enter the password to signin";
    } else if (nextProps.cardType == "forgot_password") {
      phoneOptions = true;
      firstInputPlaceholder = "Phone number";
      secondInputPlaceholder = "OTP";
      forgotButtonTitle = "Resend OTP";
      title = "You can reset your password here";
    }

    this.setState({
      phoneOptions: phoneOptions,
      firstInputPlaceholder: firstInputPlaceholder,
      secondInputPlaceholder: secondInputPlaceholder,
      forgotButtonTitle: forgotButtonTitle,
      title: title,
      cardType: nextProps.cardType
    })
  }


  commonAction = () => {

    if (this.state.cardType == "signup") {
      this.props.resendOtp()
    } else if (this.state.cardType == "signin") {
      this.forgotPassowrd();
    } else if (this.state.cardType == "signup_password") {
    }
    else if (this.state.cardType == "forgot_password") {
      this.props.resendOtp()
    }
  };

  forgotPassowrd = () => {
    this.props.navigation.navigate("ForgotPassword");
  };

  resentOtp = () => { };

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            marginTop: this.state.cardType == "forgot_password" ? 0 : 20,
            flex: 1,
            justifyContent: "flex-end",
            alignItems: "center",
            maxHeight: 50,
          }}
        >
          <Text style={SingInStyle.title}>{this.state.title}</Text>
        </View>

        {this.state.cardType == "signup_password" ? (
          <View
            style={{ marginTop: 20, minHeight: 250, justifyContent: "center" }}
          >
            <ZingTextInput
              extraStyle={{ marginBottom: 15 }}
              secureTextEntry={false}
              placeholder={"Name"}
              onChangeText={(text) => {
                this.setState({ name: text }, () => {
                  this.props.dataChanged(this.state);
                });
              }}
            ></ZingTextInput>
            <ZingTextInput
              extraStyle={{ marginBottom: 15 }}
              secureTextEntry={false}
              placeholder={"Email"}
              keyboardType={"email-address"}
              onChangeText={(text) => {
                this.setState({ email: text }, () => {
                  this.props.dataChanged(this.state);
                });
              }}
            ></ZingTextInput>
            <ZingTextInput
              extraStyle={{ marginBottom: 15 }}
              secureTextEntry={
                this.state.cardType == "signup_password" ? true : false
              }
              phoneOptions={phoneOptions}
              secureTextEntry={true}
              placeholder={this.state.firstInputPlaceholder}
              onChangeText={(text) => {
                console.log("text", text)
                this.setState({ password: text }, () => {
                  console.log("mc")
                  this.props.dataChanged(this.state);
                });
              }}
            ></ZingTextInput>
            <ZingTextInput
              secureTextEntry={true}
              placeholder={this.state.secondInputPlaceholder}
              onChangeText={(text) => {
                this.setState({ confirm_password: text }, () => {
                  this.props.dataChanged(this.state);
                });
              }}
            ></ZingTextInput>
          </View>
        ) : (
            <View
              style={{ marginTop: 20, minHeight: 200, justifyContent: "center" }}
            >
              {/* SignUp Screen - 1 Code Starts */}
              <ZingTextInput
                country_code_handler={(cc) => this.props.country_code_handler(cc)}
                extraStyle={{ marginBottom: 15 }}
                keyboardType="number-pad"
                maxLength={10}
                secureTextEntry={
                  this.state.cardType == "signup_password" ? true : false
                }
                showVerify={this.props.showVerify}
                verifyButtonHandler={() => this.props.verifyButtonHandler()}
                extraStyle={{ marginLeft: 5, marginRight: 5 }}
                phoneOptions={this.state.phoneOptions}
                secureTextEntry={false}
                placeholder={this.state.firstInputPlaceholder}
                onChangeText={(text) => {
                  this.setState({ phone: text }, () => {
                    this.props.dataChanged(this.state);
                  });
                }}
              ></ZingTextInput>
              <ZingTextInput
                keyboardType={this.state.cardType === "signin" ? "default" : "number-pad"}
                secureTextEntry={this.state.cardType === "signin" ? true : false}
                placeholder={this.state.secondInputPlaceholder}
                onChangeText={(text) => {
                  this.setState({ password: text }, () => {
                    this.props.dataChanged(this.state);
                  });
                }}
              ></ZingTextInput>
              {/* SignUp Screen - 1 Code Starts */}

            </View>

          )}

        {/* Resend OTP/ Forgot Password Button Starts */}
        {this.state.cardType == "signup_password" ? null : (
          <View style={{ flex: 1, maxHeight: 50, height: 50 }}>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "flex-start",
              }}
              onPress={() => {
                this.commonAction();
              }}
            >
              <View
                style={{ borderBottomWidth: 1, borderBottomColor: "#1492E6" }}
              >
                <Text style={{ marginBottom: 2, color: "#1492E6", fontSize: 12, fontFamily: 'Prompt-Regular' }}>
                  {this.state.forgotButtonTitle}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        )}
        {/* Resend OTP/ Forgot Password Button Ends */}

      </View>
    );
  }
}

export const SingInStyle = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    textAlign: "center",
    marginLeft: 20,
    marginRight: 20,
    lineHeight: 25,
    color: '#999999',
    fontSize: 15,
    fontFamily: 'Prompt-Regular'
  },
});
export default SignInCard;
