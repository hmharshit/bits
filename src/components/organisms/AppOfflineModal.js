import React, { Component } from "react";
import {
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    BackHandler
} from "react-native";


export default class AppOfflineModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            IsConfirmView: false,
            open: true
        }

        this.showConfirmView = this.showConfirmView.bind(this)
    }

    showConfirmView = () => {
        this.setState({
            IsConfirmView:! this.state.IsConfirmView
        })
    }

    confirmView = () => {
        return (
            <View style={{ flex: 1, marginTop: 5, borderRadius: 10 }}>
                <View style={{ flex: 1, height: 90, marginTop: 0, justifyContent: 'flex-end' }}>
                    <Text style={{ textAlign: 'center', marginBottom: 2, fontSize: 14, padding: 10, fontFamily: 'Prompt-Regular', }}>{'Cannot connect to Internet. Please Retry or Exit!'}</Text>
                </View>

                <View style={{ flex: 1, height: 100, justifyContent: 'center' }}>
                    <TouchableOpacity style={[styles.fullgreen, { justifyContent: 'center', alignContent: 'center', alignItems: 'center' }]} onPress={() => { this.props.checkInternet() }}>
                        <Text style={{ color: 'white', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Retry</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, maxHeight: 60, marginBottom: 5, justifyContent: 'flex-start', }}>
                    <TouchableOpacity style={[styles.halfgreen, { borderWidth: 0, margin: 0 }]} onPress={() => BackHandler.exitApp()}>
                        <Text style={{ color: '#8BC341', fontFamily: 'Prompt-Regular', fontSize: 15 }}>Exit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        return (
            <Modal
                style={{ flex: 1, width: '100%', width: '100%' }}
                animationType="slide"
                transparent={true}
                backgroundColor={'yellow'}
                visible={this.props.open}
                onRequestClose={() => {
                    this.props.close()
                }}
            >
                <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0, 0, 0, 0.9)' }}>
                    <View style={{ width: 300, height: 210, backgroundColor: 'white', borderRadius: 10, }}>
                        {this.confirmView()}
                    </View>
                </View>
            </Modal>
        );
    }
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        width: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'yellow'
    },
    modalView: {
        flex: 1,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    fullgreen: {
        flex:1,
        height: 50,
        maxHeight:50,
        marginLeft:20,
        marginRight:20,
        borderRadius:10,
        backgroundColor: '#8BC341',
        shadowColor: '#8BC341',
        shadowOffset: {width: 0,height: 7},
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 4,
      },
      halfgreen: {
        borderColor: '#8BC341',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10,
        maxHeight: 50,
        borderWidth:1,
        flex: 1,
        marginLeft:5,
        marginRight:5
      }
});