import React, { Component, useState } from "react";
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    TouchableHighlight,
    ActivityIndicator,
    View
} from "react-native";
import { Images, CommonStyles, Colors, Typography } from '../../styles'

export default class Loader extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { setModalVisible } = this.props;
        return (
            <Modal
                style={{ flex: 1, width: '100%', width: '100%' }}
                transparent={true}
                visible={setModalVisible}
                backgroundColor={'yellow'}
                onRequestClose={() => {

                    if(this.props.onClose) this.props.onClose()
                }}
            >
                <View style={{ flex: 1, justifyContent: 'center'}}>
                    <ActivityIndicator
                        animating={true}
                        color={Colors.PRIMARY}
                        size="large"
                        style={styles.activityIndicator} />
                </View>

            </Modal>
        );
    }
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        width: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'yellow'
    },
    modalView: {
        flex: 1,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});