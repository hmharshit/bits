import React, { Component, useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  ActivityIndicator,
  View,
  Image,
} from "react-native";
import { Images, CommonStyles, Colors, Typography } from "../../styles";
import { TextInput } from "react-native-gesture-handler";
import { Root } from 'native-base';


export default class ChangePassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false
      // setModalVisible=false
    }

  }

  componentDidMount() {
    // const { setModalVisible } = this.props;
    // this.setState({ isVisible: setModalVisible })
    // this.setState({ setModalVisible: false })
  }

  goBack = () => {
    this.props.onClose()
  }

  render() {
    const { setModalVisible } = this.props;
    // const { isVisible } = this.state

    return (
      <Modal
        style={{ flex: 1, width: "100%", width: "100%" }}
        animationType="slide"
        transparent={true}
        visible={setModalVisible}
        backgroundColor={"yellow"}
        onRequestClose={() => {
          this.props.onClose();
        }}
      >
        <Root>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            backgroundColor: "rgba(0, 0, 0, 0.5)",
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              backgroundColor: "rgba(0, 0, 0, 0.5)",
            }}
          >
            <View
              style={{
                flex: 1,
                marginLeft: 20,
                marginRight: 20,
                maxHeight: 450,
                backgroundColor: "white",
                borderRadius: 5,
                justifyContent: "center",
              }}
            >

              <TouchableOpacity
                style={styles.backIcon}
                onPress={() => this.goBack()}
              >
                <Image source={Images.back_image} />
              </TouchableOpacity>

              <Text
                style={{
                  color: "#999999",
                  fontFamily: "Prompt-Regular",
                  fontSize: 15,
                  textAlign: "center",
                  lineHeight: 20,
                }}
              >
                {"Please enter the old password and new \n password to update"}
              </Text>
              <View
                style={{
                  minHeight: 200,
                  marginLeft: 20,
                  marginRight: 20,
                  marginTop: 20,
                  justifyContent: "space-evenly",
                }}
              >
                <TextInput
                  placeholderTextColor="black"
                  textAlign={"center"}
                  placeholder={"Old Password"}
                  onChangeText={(text) => this.props.onChangeText({ old_password: text })}
                  style={{
                    flex: 1,
                    color: "#131313",
                    fontFamily: "Prompt-Regular",
                    fontSize: 15,
                    maxHeight: 50,
                    marginTop: 5,
                    marginBottom: 5,
                    borderBottomColor: Colors.UNDERLINE,
                    borderBottomWidth: 1,
                  }}
                ></TextInput>
                <TextInput
                  placeholderTextColor="black"
                  textAlign={"center"}
                  placeholder={"New Password"}
                  onChangeText={(text) => this.props.onChangeText({ new_password: text })}
                  style={{
                    flex: 1,
                    color: "#131313",
                    fontFamily: "Prompt-Regular",
                    fontSize: 15,
                    maxHeight: 50,
                    marginTop: 5,
                    marginBottom: 5,
                    borderBottomColor: Colors.UNDERLINE,
                    borderBottomWidth: 1,
                  }}
                ></TextInput>
                <TextInput
                  placeholderTextColor="black"
                  textAlign={"center"}
                  placeholder={"Confirm Password"}
                  onChangeText={(text) => this.props.onChangeText({ confirm_password: text })}
                  style={{
                    flex: 1,
                    color: "#131313",
                    fontFamily: "Prompt-Regular",
                    fontSize: 15,
                    maxHeight: 50,
                    marginTop: 5,
                    marginBottom: 5,
                    borderBottomColor: Colors.UNDERLINE,
                    borderBottomWidth: 1,
                  }}
                ></TextInput>
              </View>

              <TouchableOpacity
                style={[
                  CommonStyles.ZButton.fullgreen1,
                  CommonStyles.ZView.centerImageView,
                  { marginLeft: 20, marginRight: 20, marginTop: 40 },
                ]}
                onPress={() => {
                  this.props.updatePassword();
                }}
              >
                <Text style={{
                  color: "#FFFFFF",
                  fontFamily: "Prompt-Medium",
                  fontSize: 15,
                }}>Update</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </Root>
      </Modal >
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    width: "100%",
    width: "100%",
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "yellow",
  },
  modalView: {
    flex: 1,

    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  backIcon: {
    marginLeft: 15,
    marginBottom: 10
  },
});
