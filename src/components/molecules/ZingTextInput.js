import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import { Images, CommonStyles, Colors, Typography } from "../../styles";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import CountryPicker from 'react-native-country-picker-modal'


class ZingTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      country_code: "91",
      country_flag: "IN"
    }
  }

  sendOtp = () => { };

  render() {

    const { extraStyle, phoneOptions } = this.props;

    if (phoneOptions == true) {
      return (
        <View
          style={[
            ZTextStyle.container,
            extraStyle,
            { flexDirection: "row", justifyContent: "center" },
          ]}
        >
          <View style={{ width: 100 }}>
            <TouchableOpacity
              onPress={() => this.setState({ visible: true })}
              style={{
                flex: 1,
                // justifyContent: "flex-end",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "row",
                marginRight: -10,
              }}
            >

              <CountryPicker
                // containerButtonStyle={{fontFamily: 'Prompt-Regular'}}
                withFilter
                withCallingCode
                countryCode={this.state.country_flag}
                onClose={() => this.setState({ visible: false })}
                visible={this.state.visible}
                theme={{
                  fontFamily: 'Prompt-Regular'
                }}
                placeholder={""}
                onSelect={(country) => {
                  if (this.props.country_code_handler) {
                    this.props.country_code_handler(country['callingCode'][0])
                  }
                  this.setState({ country_code: country['callingCode'][0], visible: false, country_flag: country.cca2 })
                }}
              />
              <Text style={{ marginRight: 15, fontFamily: 'Prompt-Regular', fontSize: 15, color: '#131313' }}>
                +{this.state.country_code}
              </Text>
              <Image source={Images.down_arrow_icon} />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, marginHorizontal: 5 }}>
            <TextInput
              value={this.props.text || null}
              placeholderTextColor="black"
              keyboardType={this.props.keyboardType || "default"}
              placeholder={this.props.placeholder}
              maxLength={this.props.maxLength || 200}
              style={{
                flex: 1,
                minWidth: 150,
                textAlign: "center",
                fontSize: 15,
                fontFamily: 'Prompt-Regular',
                color: '#131313'
              }}
              secureTextEntry={this.props.secureTextEntry}
              onChangeText={(text) => this.props.onChangeText(text)}
            ></TextInput>
          </View>

          {this.props.showVerify && (
            <View style={{ width: 100 }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "row",
                }}
                onPress={() => this.props.verifyButtonHandler()}
              >
                <Text
                  style={{
                    margin: 5,
                    color: '#8BC341',
                    fontSize: 12,
                    fontFamily: 'Prompt-Regular'
                  }}
                >
                  VERIFY
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      );
    } else {
      return (
        <View
          style={[
            ZTextStyle.container1,
            extraStyle,
            { flexDirection: "row", justifyContent: "center" },
          ]}
        >
          <TextInput
            keyboardType={this.props.keyboardType || "default"}
            placeholderTextColor="black"
            placeholder={this.props.placeholder}
            maxLength={this.props.maxLength || 200}
            value={this.props.text || null}
            style={{
              // flex: 1,
              width: '100%',
              minWidth: 150,
              fontSize: 15,
              fontFamily: 'Prompt-Regular',
              color: '#131313',
              flex: phoneOptions ? 0 : 1,
              textAlign: phoneOptions ? "left" : "center",
              // fontSize: Typography.FONT_SIZE_16,
              lineHeight: 25,
            }}
            secureTextEntry={this.props.secureTextEntry}
            onChangeText={(text) => this.props.onChangeText(text)}
            editable={this.props.opt_f}
          ></TextInput>
        </View>
      );
    }
  }
}

export const ZTextStyle = StyleSheet.create({
  container: {
    flex: 1,
    maxHeight: 55,
    marginLeft: 20,
    marginRight: 20,
    borderBottomWidth: 1.5,
    borderBottomColor: Colors.UNDERLINE,
  },
  container1: {
    flex: 1,
    maxHeight: 55,
    marginLeft: 5,
    marginRight: 5,
    borderBottomWidth: 1.5,
    borderBottomColor: Colors.UNDERLINE,
  },
  textInput: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16,
  },
});

export default ZingTextInput;
