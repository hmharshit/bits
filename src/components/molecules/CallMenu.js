import React, { Component, useState } from "react";
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    TouchableHighlight
} from "react-native";
import { Images, CommonStyles, Colors, Typography } from '../../styles'

const DATA = [
    {
        icon: Images.camera_icon,
        title: 'Select the Sound Device'
    },
    {
        icon: Images.toggle_camera,
        title: 'Toggle Camera'
    },
    {
        icon: Images.enable_low_bandwidth,
        title: 'Enable Low Bandwidth Mode'
    },
    {
        icon: Images.camera_icon,
        title: 'Select the Sound Device'
    },
    {
        icon: Images.camera_icon,
        title: 'Add Meeting Password'
    },
    {
        icon: Images.start_recording,
        title: 'Start Recording'
    },
    {
        icon: Images.start_live_stream,
        title: 'Start Live Stream'
    },
    {
        icon: Images.tile_view,
        title: 'Enter Tile View'
    }, {
        icon: Images.camera_icon,
        title: 'Meeting Info'
    }, {
        icon: Images.raise_hand,
        title: 'Raise Your Hand'
    }, {
        icon: Images.blur,
        title: 'Blur Background'
    }, {
        icon: Images.muteall_icon,
        title: 'Muter All'
    },
];

export default class CallMenuScreen extends Component {

    constructor(props) {
        super(props)
    }




    _renderItem = ({ item, index }) => { // Passing as object from here
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity style={{ flex: 1, margin: 10, maxHeight: 40, flexDirection: 'row', alignItems: 'center' }} onPress = {()=>console.log('button')}>
                    <Image style={{ margin: 10}} source={item.icon} ></Image>
                    <Text style={{marginLeft:5, fontSize: Typography.FONT_SIZE_16 }}>{item.title}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const { setModalVisible } = this.props;
        return (
            <Modal
                style={{ flex: 1, width: '100%', width: '100%' }}
                animationType="slide"
                transparent={true}
                visible={setModalVisible}
                backgroundColor={'yellow'}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View onTouchStart={() => {console.log('welocme');}}  style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>        
                    <View style={{ marginLeft: 20, marginRight: 20, backgroundColor: 'white', borderRadius: 10 }}>
                        <FlatList
                            data={DATA}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={(item, index) => this._renderItem(item, index)}
                        />
                    </View>
                </View>
            </Modal>
        );
    }
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        width: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'yellow'
    },
    modalView: {
        flex: 1,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});