import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity } from 'react-native';
import { Images, CommonStyles, Colors } from '../../styles'




export default class TopBarWithoutProfile extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { navigation, renderTopIcons } = this.props
        return (

            
            <View style={{
                flex: 1, maxHeight: 80, justifyContent: 'center', flexDirection: 'row', backgroundColor: 'white',
                shadowColor: '#000000',
                shadowOffset: {
                    width: 0,
                    height: 9
                },
                shadowOpacity: 0.10,
                shadowRadius: 5.0,
                elevation: 10
            }}>
                <TouchableOpacity
                    style={{
                        width: 50, height: 73,
                        top: 5,
                        // left: 18,
                        // backgroundColor: 'red',
                        justifyContent: 'center',
                        alignItems: 'center'
                        // position: "absolute",
                    }}
                    onPress={() => this.props.navigation.pop()}
                >
                    <Image source={Images.back_image} />
                </TouchableOpacity>

                <TouchableOpacity style={{
                    flex: 1, justifyContent: 'center', alignContent: 'flex-start',
                    alignItems: 'flex-start'
                }}
                    onPress={() => {
                        if (!renderTopIcons) this.props.navigation.navigate('Home')
                    }}
                >
                    <Image style={{ left: 20 }} source={Images.logo} />
                </TouchableOpacity>
                {!renderTopIcons && <View style={{ flex: 1, backgroundColor: '', flexDirection: 'row', justifyContent: 'flex-end', alignContent: 'center', alignItems: 'center' }} >
                    {/* <TouchableOpacity style={{ backgroundColor: '', right: 10, width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() => { navigation.navigate('Setting', { animationEnabled: null }) }}>
                        <Image source={Images.setting_icon} />
                    </TouchableOpacity> */}

                    <TouchableOpacity style={{ right: 10, width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }} onPress={() =>{ navigation.navigate('Setting', { animationEnabled: null }) }}>
                        <Image source={Images.setting_icon} />

                    </TouchableOpacity>
                </View>}
            </View>
        )
    }
}




