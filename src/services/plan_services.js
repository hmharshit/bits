import API_ENDPOINTS from "./config"

export const FetchPlansList = async () => {

    let plans_list = await fetch(API_ENDPOINTS.fetch_plans.endpoint, {
        method: API_ENDPOINTS.fetch_plans.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({})
    })

    let response = await plans_list.json()
    return response
}