export const MONTH_MAPPING = {
    "January": "01", 
    "February": "02", 
    "March": "03",
    "April": "04", 
    "May": "05",
    "June": "06", 
    "July": "07",
    "August": "08",
    "September": "09",
    "October": "10",
    "November": "11", 
    "December": "12"
}

export const R_MONTH_MAPPING = {
    0: "January", 
    1: "February", 
    2:"March",
    3: "April", 
    4: "May",
    5: "June", 
    6: "July",
    7: "August",
    8: "September",
    9: "October",
    10: "November", 
    11: "December"
}

export const MEETING_LINK_INVITE_FORMAT = 
`
{name} is inviting you to a scheduled Zingallo Meeting. 
 Topic: {meeting_topic} 
 Date and Time: {meeting_time} 
 Time Zone: {time_zone} 
 
 Join Zingallo Meeting Here, 
 Meeting URL: {meeting_url} 
 Password: {password} 
 
 Or,you can use this ID; 
 Meeting ID: {meeting_id} 
 Password: {password} 
 Save your Spot. Lights. Camera. Zingallo!
`