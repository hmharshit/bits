
import API_ENDPOINTS from "./config"

import RNFetchBlob from 'rn-fetch-blob'


export const RegisterUser = async ( mobile_number, password, country_code, name, email, timezone ) => {
    const payload = {
            "mobile_number": mobile_number,
            "password": password,
            "country": country_code,
            "email": email,
            "name": name,
            "time_zone": timezone
    }

    console.log(payload)

    let register_user = await fetch(API_ENDPOINTS.register_user.endpoint, {
        method: API_ENDPOINTS.register_user.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await register_user.json()
    return response
}

export const LoginUser = async ( mobile_number, password ) => {
    const payload = {
        "mobile_number": mobile_number,
        "password": password
    }

    console.log(payload)

    let login_user = await fetch(API_ENDPOINTS.login_user.endpoint, {
        method: API_ENDPOINTS.login_user.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await login_user.json()

    return response
}


export const SendOtp = async (mobile_number, country ) => {
    const payload = {
        "mobile_number": mobile_number,
        "country": country
    }

    console.log(payload)

    let send_otp = await fetch(API_ENDPOINTS.send_otp.endpoint, {
        method: API_ENDPOINTS.send_otp.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await send_otp.json()

    return response
}


export const LoginUserGmail = async (profile_image_url, email, name, google_id) => {

    const payload = {
        "profile_img": profile_image_url,
        "email": email,
        "name": name,
        "googleID": google_id
    }

    console.log(payload)

    let login_user_gmail = await fetch(API_ENDPOINTS.login_user_gmail.endpoint, {
        method: API_ENDPOINTS.login_user_gmail.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await login_user_gmail.json()

    console.log(response)

    return response
}

export const UpdatePassword = async (user_id, password) => {

    const payload = {
        "user_id": user_id,
        "password": password
    }

    console.log(payload)

    let change_password = await fetch(API_ENDPOINTS.update_password.endpoint, {
        method: API_ENDPOINTS.update_password.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await change_password.json()
    return response
}


export const UpdateProfile = async (user_id, name, email, profile_img) => {

    let payload = {}
    if(profile_img && "data" in profile_img) {
        payload = {
            "profile_img": `data:image/jpeg;base64,${profile_img.data}`
        }
    }

    let url = `${API_ENDPOINTS.update_profile.endpoint}?user_id=${user_id}&name=${name}&email=${email}`

// console.log("222", payload.profile_img.substr(0, 10))


    let update_profile = await fetch(url, {
        method: API_ENDPOINTS.update_profile.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await update_profile.json()
    return response
}

export const ToggleNotification = async (user_id, is_enabled) => {
    const payload = {
        "user_id": user_id,
        "notification": is_enabled
    }

    console.log(payload)

    let notif_toggle = await fetch(API_ENDPOINTS.notif_toggle.endpoint, {
        method: API_ENDPOINTS.notif_toggle.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await notif_toggle.json()
    return response
}

export const ForgotPassword = async (mobile_number, country ) => {
    const payload = {
        "mobile_number": mobile_number,
        "country": country
    }

    console.log(payload)

    let send_otp = await fetch(API_ENDPOINTS.forgot_password.endpoint, {
        method: API_ENDPOINTS.forgot_password.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await send_otp.json()

    return response
}


export const ChangePassword = async ( user_id, old_password, new_password ) => {
    const payload = {
        "oldPassword": old_password,
        "newPassword": new_password,
        "user_id": user_id
    }

    console.log(payload)

    let change_password = await fetch(API_ENDPOINTS.change_password.endpoint, {
        method: API_ENDPOINTS.change_password.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await change_password.json()

    return response
}

export const LoginUserApple = async (profile_img, email, name, apple_id) => {

    const payload = {
        "profile_img": profile_img,
        "email": email,
        "name": name,
        "apple_id": apple_id,
        device_type: "ios"
    }

    console.log(payload)

    let login_user_apple = await fetch(API_ENDPOINTS.login_user_apple.endpoint, {
        method: API_ENDPOINTS.login_user_apple.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await login_user_apple.json()

    console.log(response)

    return response
}
