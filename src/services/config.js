export const BASE_URL = 'https://www.zingallo.com'
const API_HOST = BASE_URL + "/api"
const API_ENDPOINTS = {
    "send_otp": {
        "endpoint": API_HOST + '/sendOTP',
        "method": "POST"
    },
    "register_user": {
        "endpoint": API_HOST + '/signup',
        "method": "POST"
    },
    "login_user": {
        "endpoint": API_HOST + '/login',
        "method": "POST"
    },
    "login_user_gmail": {
        "endpoint": API_HOST + '/gmailLogin',
        "method": "POST"
    },
    "login_user_apple": {
        "endpoint": API_HOST + '/appleLogin',
        "method": "POST"
    },
    "forgot_password": {
        "endpoint": API_HOST + '/forgotPassword',
        "method": "POST"
    },
    "update_password": {
        "endpoint": API_HOST + '/updatePassword',
        "method": "POST"
    },
    "update_profile": {
        "endpoint": API_HOST + '/updateProfile',
        "method": "POST"
    },
    "notif_toggle": {
        "endpoint": API_HOST + '/notifToggle',
        "method": "POST"
    },
    "fetch_plans": {
        "endpoint": API_HOST + '/subscription/list',
        "method": "POST"
    },
    "fetch_timezone": {
        "endpoint": API_HOST + '/timezone',
        "method": "POST"
    },
    "fetch_meetings": {
        "endpoint": API_HOST + '/meeting/dashboardList',
        "method": "POST"
    },
    "meeting_jwt_auth": {
        "endpoint": API_HOST + '/meeting/jwtAuth',
        "method": "POST"
    },
    "meeting_guest_auth": {
        "endpoint": API_HOST + '/meeting/joinAuth',
        "method": "POST"
    },
    "start_meeting_timezone": {
        "endpoint": API_HOST + '/meeting/autocreate',
        "method": "POST"
    },
    "meeting_check_count": {
        "endpoint": API_HOST + '/meeting/join',
        "method": "POST"
    },
    "schedule_meeting": {
        "endpoint": API_HOST + '/meeting/create',
        "method": "POST"
    },
    "update_schedule_meeting": {
        "endpoint": API_HOST + '/meeting/edit',
        "method": "POST"
    },
    "delete_meeting": {
        "endpoint": API_HOST + '/meeting/delete',
        "method": "POST"
    },
    "meeting_invitation": {
        "endpoint": API_HOST + '/meeting/invitation',
        "method": "POST"
    },
    "check_subscription_plan": {
        "endpoint": API_HOST + '/meeting/subscription',
        "method": "POST"
    },
    "update_firebase_token": {
        "endpoint": API_HOST + '/firebaseToken',
        "method": "POST"
    },
    "change_password": {
        "endpoint": API_HOST + '/changepassword',
        "method": "POST"
    }
}

export default API_ENDPOINTS;