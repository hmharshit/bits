import API_ENDPOINTS from "./config"
import { useEffect } from "react"

export const FetchTimezoneList = async () => {

    let timezone_list = await fetch(API_ENDPOINTS.fetch_timezone.endpoint, {
        method: API_ENDPOINTS.fetch_timezone.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({})
    })

    let response = await timezone_list.json()
    return response
}


export const UpdateFirebaseToken = async (user_id, token) => {

    const payload = {
        user_id: user_id,
        token: token
    }

    let firebase_token = await fetch(API_ENDPOINTS.update_firebase_token.endpoint, {
        method: API_ENDPOINTS.update_firebase_token.method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload)
    })

    let response = await firebase_token.json()
    return response
}