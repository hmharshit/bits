//Welcome Screen
export const logo = require('../assets/images/logo_landing.png')
export const mobImage = require('../assets/images/img.png')
export const chat_icon = require('../assets/images/chat_icon.png')
export const menu_icon = require('../assets/images/menu_icon.png')
export const camera_icon = require('../assets/images/camera_icon.png')
export const back_icon = require('../assets/images/back_icon.png')
export const setting_icon = require('../assets/images/settings_icon.png')
export const user_icon = require('../assets/images/user_icon.png')
export const video_icon = require('../assets/images/joinameeting_dashboard_icon.png')
export const calendar_icon = require('../assets/images/scheduleameeting_dashboard_icon.png')
export const down_arrow_icon = require('../assets/images/down_arrow_icon.png')
export const waiting_host_icon = require('../assets/images/waiting_host_icon.png')
export const toggle_on_icon = require('../assets/images/toggle_on_icon.png')
export const toogle_off_icon = require('../assets/images/toogle_off_icon.png')
export const google = require('../assets/images/google.png')
export const apple = require('../assets/images/apple_icon.png')


export const audio_mute_icon = require('../assets/images/mute.png')
export const video_unmute_icon = require('../assets/images/video_icon.png')
export const endcall_icon = require('../assets/images/endcall_icon.png')

export const copy = require('../assets/images/copy.png')
export const addtoicall = require('../assets/images/addtoicall.png')
export const agenda = require('../assets/images/agenda.png')
export const addtooutlook = require('../assets/images/addtooutlook.png')


export const profileImage = require('../assets/images/profileImage.png')
export const video_image = require('../assets/images/video_image.png')
export const audiocall_speaker_icon = require('../assets/images/call_speaker_icon.png')
export const audiocall_admin_icon = require('../assets/images/call_admin_icon.png')

export const audiocall_mute_icon = require('../assets/images/call_mute_icon.png')
export const videocall_mute_icon = require('../assets/images/mute_call_video_icon.png')

export const ismile1 = require('../assets/images/i-smile.png')
export const isend1 = require('../assets/images/i-send.png')


export const toggle_camera = require('../assets/images/ToggleCamera.png')
export const enable_low_bandwidth = require('../assets/images/Enable_Low_Bandwidth_Mode.png')
export const start_live_stream = require('../assets/images/Start_Live_Stream.png')
export const start_recording = require('../assets/images/Start_Recording.png')
export const tile_view = require('../assets/images/Enter_Tile_View.png')
export const raise_hand = require('../assets/images/raise_hand.png')
export const blur = require('../assets/images/blur.png')
export const muteall_icon = require('../assets/images/muteall_icon.png')

export const pricing_grey = require('../assets/images/pricing_grey.png')
export const pricing_participant = require('../assets/images/pricing_participant.png')
export const pricing_time = require('../assets/images/pricing_time.png')
export const pricing_space = require('../assets/images/pricing_space.png')
export const payment_success_icon = require('../assets/images/payment_success_icon.png')
export const default_profile_image = require('../assets/images/default_dp.png')

export const edit_image = require('../assets/images/edit.png')
export const share_image = require('../assets/images/share.png')
export const copy_image_wb = require('../assets/images/copy_wb.png')

export const video_call_image = require('../assets/images/video-call.png')

export const back_image = require('../assets/images/back.png')