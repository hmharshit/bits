import { StyleSheet, Dimensions } from 'react-native';
import * as Colors from './colors';

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

export const ZButton = StyleSheet.create({
  fullgreen: {
    height: DeviceHeight / 100 * 30,
    width: DeviceWidth / 100 * 90,
    maxHeight: 50,
    margin: 20,
    borderRadius: 10,
    backgroundColor: Colors.PRIMARY,
    shadowColor: Colors.PRIMARY,
    shadowOffset: { width: 0, height: 7 },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 4,
  },
  fullgreen1: {
    height: DeviceHeight / 100 * 30,
    width: DeviceWidth / 100 * 80,
    maxHeight: 50,
    margin: 20,
    borderRadius: 10,
    backgroundColor: Colors.PRIMARY,
    shadowColor: Colors.PRIMARY,
    shadowOffset: { width: 0, height: 7 },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 4,
  },
  fullgreen2: {
    height: DeviceHeight / 100 * 30,
    width: DeviceWidth / 100 * 90,
    maxHeight: 50,
    marginLeft: 20,
    marginBottom: 10,
    marginRight: 20,
    borderRadius: 10,
    backgroundColor: Colors.PRIMARY,
    shadowColor: Colors.PRIMARY,
    shadowOffset: { width: 0, height: 7 },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,
    elevation: 4,
  },

  fullred: {
    height: DeviceHeight / 100 * 30,
    width: DeviceWidth / 100 * 90,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 10,
    maxHeight: 50,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    borderRadius: 10,

  },
  halfgreen: {
    height: DeviceHeight / 100 * 30,
    width: DeviceWidth / 100 * 43,
    borderColor: Colors.PRIMARY,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    maxHeight: 50,
    borderWidth: 1,
    margin: 5
  }
});

export const ZView = StyleSheet.create({
  container: {
    flex: 1,
  },
  container1: {
    height: DeviceHeight / 100 * 15,
    width: DeviceWidth / 100 * 90,
    marginLeft: 20,
    marginRight: 20,


  },
  container2: {
    height: DeviceHeight / 100 * 7,
    width: DeviceWidth / 100 * 90,
    marginLeft: 20,
    marginRight: 20,
  },
  centerView: {
    flex: 3,
  },
  centerView1: {
    height: DeviceHeight / 100 * 55,
    width: DeviceWidth / 100 * 90,
    marginLeft: 20,
    marginRight: 20,
  },
  centerView2: {
    height: DeviceHeight / 100 * 50,
    width: DeviceWidth / 100 * 90,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    marginTop: 80,

  },
  contentView: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
  },
  logoView: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 20
  },
  logoNewView: {
    alignItems: 'center',
    marginBottom: 20,
    flexDirection: 'row',
  },
  centerImageView: {
    justifyContent: 'center',
    alignItems: 'center',
  }
});
