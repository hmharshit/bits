import React, { Component } from 'react';
import {
  SafeAreaView, View, Image, Text, TouchableOpacity,
  AsyncStorage, StyleSheet, Dimensions
} from 'react-native';
import Loader from '../../components/molecules/Loader'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from "react-redux"
import { RegisterUser } from "../../services/auth_services"
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import ZingTextInput from '../../components/molecules/ZingTextInput'
import { Dropdown } from 'react-native-material-dropdown'
import { FetchTimezoneList } from "../../services/general_services"
import { Notification } from "../../components/util"
import { setIsUserLoggedIn, setUserData } from "../../actions/home"
import DropDownModal from "../../components/organisms/DropDownModal"

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

class PasswordScreen extends Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true

    this.state = {
      phone: '',
      password: '',
      IsLoader: false,
      timezones: []
    }

    this.onSigninDidSelected = this.onSigninDidSelected.bind(this)
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }



  storeUserDetails = async (user) => {
    console.log("storing user details")
    await AsyncStorage.setItem("user", JSON.stringify(user))
    this.props.dispatch(setUserData(user))
    this.props.dispatch(setIsUserLoggedIn(true))
    this.props.navigation.navigate("Home")

  }


  onSigninDidSelected = () => {

    let { name, password, confirm_password, email, selected_timezone } = this.state
    let phone = this.props.navigation.getParam('mobile', '')

    if (!name || !password || !confirm_password || !email || !selected_timezone.Value) {
      Notification("All fields are mandatory!", "warning", 0)
      return false
    }

    if(!this.validateEmail(email)) {
      Notification("Email ID is not valid!", "error", 0)
      return false
    }

    if (!this.checkPassword()) {
      Notification("Passwords don't match!", "error", 0)
      return false
    }

    this.setState({ IsLoader: true })

    RegisterUser(phone, password,
      this.props._countryCode, name, email, selected_timezone.Value).then(resp => {
        this.setState({ IsLoader: false });
        if (resp && !resp.error) {
          resp = resp.result
          if (resp.message && resp.message == "User logged in sucessfully.") {
            console.log("user_authenticated")
            Notification("Sign Up successful", "info")
            this.storeUserDetails(resp.user)
          }
        }
        else {
          Notification(resp.result.message, "error")
        }
      }).catch(err => {
        this.setState({ IsLoader: false });
        console.log(err)})
  }

  checkPassword() {
    const {
      password,
      confirm_password
    } = this.state

    if (password === confirm_password) return true
    else return false
  }

  componentDidMount() {
    this.setState({ IsLoader: true })
    FetchTimezoneList().then(response => {

      if (response && !response.error) {
        response = response.result;

        if (response && response.message === "timezone list") {
          let timezones = [], timezone_mapping = {};

          response.timezone.forEach((tz, i) => {
            timezones.push({
              Value: tz.timezone,
              Name: tz.utctime,
              Id: i +1
            })
            timezone_mapping[tz.utctime] = tz.timezone
          })

          this.setState({
            timezones: timezones,
            timezone_mapping: timezone_mapping,
            IsLoader: false,
            selected_timezone: {
              Name: "Select Timezone",
              Value: null
            }
          })

        }
      }
    })
  }

  render() {

    const { timezones } = this.state

    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <KeyboardAwareScrollView style={{ flex: 1, width: '100%' }} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
          <View style={[CommonStyles.ZView.container, { justifyContent: 'flex-end', borderColor: 'red' }]}>

            <View style={[CommonStyles.ZView.container, CommonStyles.ZView.logoNewView]}>
              <TouchableOpacity style={{ marginLeft: 10, marginRight: 10 }}
                onPress={() => this.props.navigation.pop()}
              >
                <Image source={Images.back_image} style={{ top: 15 }} />
              </TouchableOpacity>

              <Image source={Images.logo} style={{
                justifyContent: 'center', alignItems: 'center', position: 'absolute',
                left: DeviceWidth / 100 * 26, top: DeviceHeight / 100 * 10
              }} />
            </View>

            <View style={[CommonStyles.ZView.centerView, { justifyContent: 'center', flex: 1, marginTop: 50 }]}>
              <View style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ textAlign: 'center', color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15 }}>{"We have successfully verified your mobile \n Please enter the details to Sign Up"}</Text>
              </View>
              <ZingTextInput extraStyle={{ marginTop: 20, marginBottom: 10, fontFamily: 'Prompt-Regular', fontSize: 15, color: '#131313' }} phoneOptions={false} secureTextEntry={false} placeholder={"Name"} onChangeText={(text) => { this.setState({ name: text }) }} ></ZingTextInput>
              <ZingTextInput extraStyle={{ marginBottom: 10, fontFamily: 'Prompt-Regular', fontSize: 15, color: '#131313' }} keyboardType={"email-address"} phoneOptions={false} secureTextEntry={false} placeholder={"Email"} onChangeText={(text) => { this.setState({ email: text }) }}></ZingTextInput>
              <ZingTextInput extraStyle={{ marginBottom: 10, fontFamily: 'Prompt-Regular', fontSize: 15, color: '#131313' }} phoneOptions={false} secureTextEntry={true} placeholder={"New Password"} onChangeText={(text) => { this.setState({ password: text }) }} ></ZingTextInput>
              <ZingTextInput extraStyle={{ marginBottom: 10, fontFamily: 'Prompt-Regular', fontSize: 15, color: '#131313' }} phoneOptions={false} secureTextEntry={true} placeholder={"Confirm Password"} onChangeText={(text) => { this.setState({ confirm_password: text }) }} ></ZingTextInput>

              {/* <View style={{ flexDirection: 'row', height: '10%' }}> */}
      
              <View
              style={{
                flexDirection: "column",
                marginTop: 5,
                height: DeviceHeight / 100 * 7,
                marginLeft: 13,
                borderBottomColor: Colors.UNDERLINE,
                borderBottomWidth: 1,
              }}
            >
          <DropDownModal
            data={timezones}
            onClose={() => null}
            selected={this.state.selected_timezone}
            onSelect={(selected) => Object.keys(selected).length > 0 && this.setState({ selected_timezone: selected })}
            onBackButton={() => null}
            render={(disabled, selected, showModal) =>

              <TouchableOpacity style={{ flexDirection: 'row', height: DeviceHeight / 100 * 10, marginTop: 10, justifyContent: "center" }} onPress={showModal}>
                  <Text
        style={{fontSize: 15, fontFamily: 'Prompt-Regular', textAlign: 'center'}}
                  >{this.state.selected_timezone && this.state.selected_timezone.Name}
                </Text></TouchableOpacity>}
          /></View>
              {/* </View> */}

              <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView]} onPress={this.onSigninDidSelected}>
                <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Sign Up</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Loader setModalVisible={this.state.IsLoader} ></Loader>

      </SafeAreaView>
    )
  }
}

const _mapStateToProps = (state) => {
  console.log(state)
  return {
    _mobileNumber: state["register"].mobile_number,
    _countryCode: state["register"].country_code
  }
}

export default connect(_mapStateToProps)(PasswordScreen);


export const passwordStyles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    textAlign: 'center',
    width: 330,
    height: 100,
    marginBottom: 10,
    lineHeight: 25,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16
  },
});