import React, { Component, useState } from "react";
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    View,
    Image
} from "react-native";
import { Images, CommonStyles, Colors, Typography } from '../../styles'

export default class HostingScreen extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { setModalVisible } = this.props;
        return (
            <Modal
                style={{ flex: 1, width: '100%', width: '100%' }}
                animationType="slide"
                transparent={true}
                visible={setModalVisible}
                backgroundColor={'yellow'}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                    <View style={{ flex: 1, marginLeft: 20, marginRight: 20, maxHeight: 215, backgroundColor: 'white', borderRadius: 5, justifyContent: 'center', alignItems: 'center' }}>
                        <Image style={{ marginBottom: 10 }} source={Images.waiting_host_icon} />
                        <Text style={{ marginTop: 10, marginBottom: 10, color: '#8BC341', fontSize: 15, fontFamily: 'Prompt-Regular' }}>WAITING FOR THE HOST</Text>
                        <Text style={{ color: '#131313', fontSize: 12, fontFamily: 'Prompt-Regular' }}>{'The Conference 2808 1989 has not yet started.\n If you are the host then please authenticate.\n Otherwise, please wait for the host to arrive.'}</Text>
                    </View>
                </View>
            </Modal>
        );
    }
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        width: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'yellow'
    },
    modalView: {
        flex: 1,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});