import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  Platform
} from "react-native";
import { Images, CommonStyles, Colors } from "../../styles";
import SigninCard from "../../components/organisms/SigninCard";
import GoogleSignInIcon from "../../components/organisms/GoogleSignIn";
import AppleSignIn from "../../components/organisms/AppleSignIn"
import Loader from "../../components/molecules/Loader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { LoginUser } from "../../services/auth_services"
import { Notification } from "../../components/util";
import { setIsUserLoggedIn, setUserData } from "../../actions/home"
import { connect } from "react-redux"

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    console.disableYellowBox = true;

    this.state = {
      phone: "",
      password: "",
      IsLoader: false,
    };

    this.onSigninDidSelected = this.onSigninDidSelected.bind(this);
  }


  onStateUpdated(data) {
    const { phone, password } = { data };
    this.setState({ phone: data.phone, password: data.password });
    console.log(this.state);
  }


  onSigninDidSelected() {
    this.setState({ IsLoader: true })
  }

  storeUserDetails = async (user) => {
    console.log("storing user details")
    await AsyncStorage.setItem("user", JSON.stringify(user))
    this.props.dispatch(setUserData(user))
    this.props.dispatch(setIsUserLoggedIn(true))
    this.props.navigation.navigate("Home")

  }

  checkValidNumber(number) {
    if (number.indexOf(".") == -1 && number.length == 10) return true
    else return false
  }

    isSupported = () => {
    const majorVersionIOS = parseInt(Platform.Version, 10)
    return majorVersionIOS >=13 && Platform.OS === "ios" ? true : false
  }



  onSigninDidSelected() {

    if (!this.state.phone) {
      Notification("Please enter the mobile number!", "warning", 0)
      return false
    }

    if (!this.checkValidNumber(this.state.phone)) {
      Notification("Please enter a valid number!", "warning", 0)
      return false
    }

    if(!this.state.password) {
      Notification("Please enter password", "warning", 0)
      return false
    }

    this.setState({ IsLoader: true });


    LoginUser(this.state.phone, this.state.password).then(resp => {
      console.log(resp)
      this.setState({ IsLoader: false });

      if (resp && !resp.error) {
        console.log("here")
        resp = resp.result
        if (resp.message && resp.message === "User logged in sucessfully.") {
          console.log("here1")
          console.log("user_authenticated")
          this.storeUserDetails(resp.user)
          Notification("Successfully Logged In!", "success")
        }
      }
      else {
        Notification(resp.result.message, "error", 0)
      }

    }).catch(err => {
      this.setState({ IsLoader: false });
      console.log(err)})

    // this.props.navigation.navigate("Home");

  }

  render() {
    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <KeyboardAwareScrollView
          style={{ flex: 1, width: "100%" }}
          contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
        >
          <View style={CommonStyles.ZView.container}>
            <View
              style={[
                CommonStyles.ZView.container,
                CommonStyles.ZView.logoNewView,
              ]}
            >
              <TouchableOpacity style={{ marginLeft: 20, marginRight: 10, marginTop: 20  }}
                onPress={() => this.props.navigation.pop()}
              >
                <Image source={Images.back_image} />
              </TouchableOpacity>

              <Image source={Images.logo} style={{
                justifyContent: 'center', alignItems: 'center', position: 'absolute',
                left: DeviceWidth / 100 * 26, top: DeviceHeight / 100 * 8.7
              }} />
            </View>

            <View style={[CommonStyles.ZView.centerView2]}>
              <SigninCard
                country_code_handler={(cc) => this.setState({ country_code: cc })}
                navigation={this.props.navigation}
                cardType={"signin"}
                dataChanged={(data) => {
                  this.onStateUpdated(data);
                }}
              />
            </View>

            <View
              style={[
                CommonStyles.ZView.container,
                { justifyContent: "center", marginBottom: 10 },
              ]}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                }}
              >
                <TouchableOpacity
                  style={[
                    CommonStyles.ZButton.fullgreen2,
                    CommonStyles.ZView.centerImageView,
                  ]}
                  onPress={this.onSigninDidSelected}
                >
                  <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Sign In</Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom: 10
                }}
              >
                <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 12 }}>
                  Or Sign In with
                </Text>
              </View>

              {this.isSupported() &&  <AppleSignIn
                  navigation={this.props.navigation}
                  text={"Sign In with Apple"}
              />}

              <GoogleSignInIcon
                navigation={this.props.navigation}
                text={"Sign In with Google"}
                style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Loader setModalVisible={this.state.IsLoader}></Loader>
      </SafeAreaView>
    );
  }
}

const _mapStateToProps = (state) => {
  return {

  }
}

export default connect(_mapStateToProps)(LoginScreen);
