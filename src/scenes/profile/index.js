import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  AsyncStorage,
  Dimensions
} from "react-native";
import { Images, CommonStyles, Colors, Typography } from "../../styles";
import Loader from "../../components/molecules/Loader";
import SingOutPopup from "../../components/organisms/SignoutModal";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import TopBar from "../../components/atoms/TopBarWithoutProfile";
import ZingTextInput from "../../components/molecules/ZingTextInput";
import ChangePassword from "../../components/molecules/ChangePassword";
import ProfileTextInput from "../../components/molecules/ProfileTextInput";
import ImagePicker from "react-native-image-picker";

import { ChangePassword as CP, UpdateProfile } from "../../services/auth_services";
import { setIsUserLoggedIn, setUserData } from "../../actions/home";
import { connect } from "react-redux";
import { BASE_URL } from "../../services/config"

import { Notification } from "../../components/util";
const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

const options = {
  maxWidth: 1280,
  maxHeight: 720,
  title: "Select Avatar",
  storageOptions: {
    skipBackup: true,
    path: "/",
  },
};

class ProfileScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      account: "",
      password: "",
      IsEditOn: false,
      IsLoader: false,
      IsPasswordShow: false,
      openSignout: false,
    };

    this.onSigninDidSelected = this.onSigninDidSelected.bind(this);
  }

  _updatePassword = () => {
    const {
      old_password,
      new_password,
      confirm_password,
      user_id,
    } = this.state;

    if (!old_password || !new_password || !confirm_password || !user_id) {
      Notification("All fields are mandatory", "warning", 0);
      return false;
    }

    if (!old_password.trim() || !new_password.trim() || !confirm_password.trim()) {
      Notification("Please enter correct values", "warning", 0);
      return false;
    }

    if (new_password !== confirm_password) {
      // if passwords are not same
      Notification("New Password doesn't match", "error", 0);
      return false;
    }

    this.setState({ IsLoader: true });

    CP(user_id, old_password, new_password).then((response) => {
      if (response && !response.error) {
        Notification(response.result.message, "success", 0);
        this.setState({ IsPasswordShow: false });
      } else {
        Notification(response.result.message, "error", 0);
      }

      this.setState({ IsLoader: false });
    });
  };

  _updateProfile = () => {
    this.setState({ IsLoader: true });

    const { name, user_id, account, tmp_profile_image } = this.state;

    // console.log(this.state);

    if (!name || !user_id) {
      Notification("All fields are mandatory", "warning", 0);
      return false;
    }

    UpdateProfile(user_id, name, account, tmp_profile_image).then((response) => {
      this.setState({ IsLoader: false, IsEditOn: false });

      console.log(response);
      if (response && !response.error) {
        response = response.result;

        if (response.message === "Profile updated sucessfully.") {
          AsyncStorage.setItem("user", JSON.stringify(response.user));
        }
      }
    }).catch(err => {
      this.setState({ IsLoader: false });
      console.log(err)
    });
  };

  fetchUserDetails = () => {
    AsyncStorage.getItem("user").then((user) => {
      user = JSON.parse(user);

      this.setState({
        name: user.name,
        account: user.email,
        mobile: user.mobile,
        profile_image: user.profile_img,
        user_id: user.id,
      });
    });
  };

  componentDidMount() {
    this.fetchUserDetails();
  }

  onSigninDidSelected() {
    console.log("suresh");
    console.log(this.state);
    this.setState({
      IsLoader: true,
    });

    setTimeout(
      function () {
        this.setState({ IsLoader: false });
        this.props.navigation.navigate("Home");
      }.bind(this),
      1000
    );
  }

  signOutUser = async () => {
    await AsyncStorage.clear();

    this.props.dispatch(setIsUserLoggedIn(false));
    this.props.dispatch(setUserData({}));

    Notification("You have been signed out!", "success");

    this.props.navigation.navigate("Auth");
  };

  render() {
    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <TopBar navigation={this.props.navigation}></TopBar>
        <KeyboardAwareScrollView
          style={{ flex: 1, width: "100%" }}
          contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
        >
          <View style={[CommonStyles.ZView.container, {}]}>
            {/* <View style={styles.header}> */}


            <View style={styles.header}>
              <View style={{ flex: 1 }} >

              </View>
              <View style={{ width: 130, height: 130, justifyContent: 'center', alignItems: 'center' }} >
                <Image style={{ width: 110, height: 110 }}
                  source={
                    this.state.profile_image
                      ? {
                        uri: `${BASE_URL}/uploads/userimage/${this.state.profile_image}`,
                      }
                      : this.state.tmp_profile_image
                        ? this.state.tmp_profile_image
                        : Images.default_profile_image
                  }
                />




                {this.state.IsEditOn && (
                  <TouchableOpacity
                    style={styles.cameraIcon}
                    onPress={() => {
                      ImagePicker.showImagePicker(options, (response) => {
                        console.log("Response = ", Object.keys(response));
                        // console.log("Response = ", response.data);

                        if (response.didCancel) {
                          console.log("User cancelled image picker");
                        } else if (response.error) {
                          console.log(
                            "ImagePicker Error: ",
                            response.error
                          );
                        } else if (response.customButton) {
                          console.log(
                            "User tapped custom button: ",
                            response.customButton
                          );
                        } else {
                          const source = { uri: response.uri, type: response.type, name: response.fileName, data: response.data };

                          // You can also display the image using data:
                          // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                          this.setState({ profile_image: null }, () => {
                            this.setState({
                              tmp_profile_image: source,
                            });
                          });
                        }
                      });
                    }}
                  >
                    <Image source={Images.camera_icon} />
                  </TouchableOpacity>
                )}




              </View>
              <View style={{ flex: 1 }} >
                {this.state.IsEditOn == false ? (
                  <TouchableOpacity
                    style={{ flex: 1, alignItems: 'flex-end', right: 15 }}
                    onPress={() => {
                      this.setState({ IsEditOn: true });
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 15,
                        color: "#8BC341",
                        fontFamily: "Prompt-Regular",
                        textAlign: 'center',
                      }}
                    >
                      EDIT
                      </Text>
                  </TouchableOpacity>
                ) : null}
              </View>
            </View>

            {/* <View style={{ flexDirection: "row", backgroundColor:'yellow' }}> */}



            {/* <View style={{ flex: 1 }}>
                  <TouchableOpacity
                    style={styles.backIcon}
                    onPress={() => this.props.navigation.pop()}
                  >
                    <Image source={Images.back_icon} />
                  </TouchableOpacity>
                </View> */}
            {/* <View style={{ alignItems: "center", height: DeviceHeight / 100 * 20, width: DeviceWidth, justifyContent: 'center', flexDirection: 'row' }}>
                  <View style={{ height: DeviceHeight / 100 * 20, width: DeviceWidth / 100 * 70, alignItems: 'flex-end', flexDirection: 'column', justifyContent: 'center' }}>
                    <Image
                      style={{ width: 110, height: 110, borderRadius: 15, margin: 30 }}
                      source={
                        this.state.profile_image
                          ? {
                            uri: `${BASE_URL}/uploads/userimage/${this.state.profile_image}`,
                          }
                          : this.state.tmp_profile_image
                            ? this.state.tmp_profile_image
                            : Images.default_profile_image
                      }
                    />
                    {this.state.IsEditOn && (
                      <TouchableOpacity
                        style={styles.cameraIcon}
                        onPress={() => {
                          ImagePicker.showImagePicker(options, (response) => {
                            console.log("Response = ", Object.keys(response));
                            // console.log("Response = ", response.data);

                            if (response.didCancel) {
                              console.log("User cancelled image picker");
                            } else if (response.error) {
                              console.log(
                                "ImagePicker Error: ",
                                response.error
                              );
                            } else if (response.customButton) {
                              console.log(
                                "User tapped custom button: ",
                                response.customButton
                              );
                            } else {
                              const source = { uri: response.uri, type: response.type, name: response.fileName, data: response.data };

                              // You can also display the image using data:
                              // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                              this.setState({ profile_image: null }, () => {
                                this.setState({
                                  tmp_profile_image: source,
                                });
                              });
                            }
                          });
                        }}
                      >
                        <Image source={Images.camera_icon} />
                      </TouchableOpacity>
                    )}
                  </View>
                  <View style={{ height: DeviceHeight / 100 * 20, width: DeviceWidth / 100 * 30, flexDirection: 'column' }}>
                    {this.state.IsEditOn == false ? (
                      <TouchableOpacity
                        style={{ flex: 1 }}
                        onPress={() => {
                          this.setState({ IsEditOn: true });
                        }}
                      >
                        <Text
                          style={{
                            color: "#8BC341",
                            fontSize: 15,
                            fontFamily: "Prompt-Regular",
                            marginRight: 20,
                            textAlign: 'center',
                            marginTop: 50,
                            marginRight: 40
                          }}
                        >
                          EDIT
                      </Text>
                      </TouchableOpacity>
                    ) : null}
                  </View>
                </View> */}

            {/* </View> */}
            {/* </View> */}

            <View style={{ flex: 1, marginTop: 20 }}>
              <ProfileTextInput
                placeHolder={"Account"}
                text={this.state.account}
                editable={false}
                onChangeText={(text) => {
                  this.setState({ account: text });
                }}
              ></ProfileTextInput>
              <ProfileTextInput
                placeHolder={"Display Name"}
                text={this.state.name}
                editable={this.state.IsEditOn}
                onChangeText={(text) => {
                  this.setState({ name: text });
                }}
              ></ProfileTextInput>
              {
                !this.props._isGmailUser &&
                <ProfileTextInput
                  placeHolder={"Password"}
                  text={"************"}
                  secureTextEntry={true}
                  editable={false}
                  onChangeText={(text) => {
                    this.setState({ password: text });
                  }}
                ></ProfileTextInput>}

              {this.state.IsEditOn && !this.props._isGmailUser ? (
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignSelf: "flex-end",
                    width: 120,
                    margin: 10,
                    marginRight: 20,
                    maxHeight: 40,
                    justifyContent: "center",
                    alignItems: "flex-end",
                  }}
                  onPress={() => {
                    this.setState({ IsPasswordShow: true });
                  }}
                >
                  <Text
                    style={{
                      color: "#8BC341",
                      fontSize: 15,
                      fontFamily: "Prompt-Regular",
                    }}
                  >
                    CHANGE
                  </Text>
                </TouchableOpacity>
              ) : null}

              {this.state.IsEditOn ? (
                <TouchableOpacity
                  style={[
                    CommonStyles.ZButton.fullgreen,
                    CommonStyles.ZView.centerImageView,
                    { marginLeft: 20, marginRight: 20, marginTop: 50 },
                  ]}
                  onPress={() => {
                    this._updateProfile();
                  }}
                >
                  <Text
                    style={{
                      color: "#FFFFFF",
                      fontSize: 15,
                      fontFamily: "Prompt-Regular",
                    }}
                  >
                    Update
                  </Text>
                </TouchableOpacity>
              ) : (
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      maxHeight: 50,
                      marginTop: 50,
                      justifyContent: "flex-start",
                      alignItems: "center",
                    }}
                    onPress={() => {
                      this.setState({ openSignout: true })
                    }}
                  >
                    <Text
                      style={{
                        color: "#8BC341",
                        fontSize: 15,
                        fontFamily: "Prompt-Regular",
                      }}
                    >
                      SIGN OUT
                  </Text>
                  </TouchableOpacity>
                )}
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Loader setModalVisible={this.state.IsLoader}></Loader>
        <SingOutPopup open={this.state.openSignout}
          signout={() => this.signOutUser()}
          close={() => this.setState({ openSignout: false })} />
        <ChangePassword
          updatePassword={() => {
            this._updatePassword();
          }}
          onChangeText={(data) => {
            if (data.new_password)
              this.setState({ new_password: data.new_password });
            if (data.old_password)
              this.setState({ old_password: data.old_password });
            if (data.confirm_password)
              this.setState({ confirm_password: data.confirm_password });
          }}
          navigation={this.props.navigation}
          setModalVisible={this.state.IsPasswordShow}
          onClose={() => {
            this.setState({ IsPasswordShow: false });
          }}
        />
      </SafeAreaView>
    );
  }
}

const _mapStateToProps = (state) => {


  let gmail_id = state["home"].user_data.gmail_id, _isGmailUser
  _isGmailUser = gmail_id && typeof gmail_id === "string" && gmail_id.length > 2 ? true : false

  return {
    _userId: state["home"].user_data.id,
    _isGmailUser: _isGmailUser
  };
};

export default connect(_mapStateToProps)(ProfileScreen);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    textAlign: "center",
    width: 330,
    height: 100,
    marginBottom: 10,
    lineHeight: 25,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16,
  },
  header: {
    flex: 1,
    marginTop: 40,
    maxHeight: 130,
    justifyContent: "center",
    flexDirection: 'row',
  },
  backIcon: {
    width: 40,
    height: 40,
    position: "absolute",
  },
  cameraIcon: {
    right: 0,
    bottom: 0,
    width: 40,
    height: 40,
    position: "absolute",
  },

  container: {
    flex: 1,
    maxHeight: 55,
    marginLeft: 20,
    marginRight: 20,
    borderBottomWidth: 1.5,
    borderBottomColor: Colors.UNDERLINE,
  },
  textInput: {
    fontFamily: Typography.FONT_FAMILY_REGULAR,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16,
  },
});
