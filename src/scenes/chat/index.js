import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import { FONT_WEIGHT_BOLD } from '../../styles/typography';
import { Dropdown } from 'react-native-material-dropdown'
import { BottomSheet } from 'react-native-btr';

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

export default class chat extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true

        this.state = {
            phone: '',
            password: '',
            visible: false,
            IsLoader: false,
            code: [
                { value: 'Everyone' },
                { value: 'Only Me' },
            ],
        }
        this.onSigninDidSelected = this.onSigninDidSelected.bind(this)
    }

    onStateUpdated(data) {
        const { phone, password } = { data }
        this.setState({ phone: data.phone, password: data.password })
    }

    onSigninDidSelected() {
        this.setState({ IsLoader: true })

        setTimeout(
            function () {
                this.setState({ IsLoader: false });
                this.props.navigation.navigate('Call')
            }.bind(this),
            1000
        );
    }

    componentDidMount = () => {

    }

    _toggleBottomNavigationView = () => {
        // alert(JSON.stringify(this.state.coCreatorData))
        this.setState({ visible: !this.state.visible });
    }

    render() {
        const { code } = this.state
        return (

            <View style={{ height: DeviceHeight, width: DeviceWidth }}>
                {/* header */}
                <View style={{
                    height: DeviceHeight / 100 * 10, width: DeviceWidth, flexDirection: 'row', backgroundColor: 'white',
                    shadowColor: '#000000',
                    shadowOffset: {
                        width: 0,
                        height: 9
                    },
                    shadowOpacity: 0.46,
                    shadowRadius: 9.51,
                    elevation: 10
                }}>
                    <View style={{ height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ marginLeft: -7 }} onPress={() => this.props.navigation.pop()}>
                            <Image source={Images.back_icon} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 60, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: '#131313', fontFamily: 'Prompt-SemiBold', fontSize: 16 }} >Personal Meeting</Text>
                        <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 16 }}>{'1:28:45'}</Text>
                    </View>
                    <View style={{ height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ marginLeft: 7 }}
                            onPress={this._toggleBottomNavigationView}>
                            <Image source={Images.menu_icon} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ height: DeviceHeight / 100 * 5, width: DeviceWidth / 100 * 60, flexDirection: 'row' }}>
                    <View style={{ height: DeviceHeight / 100 * 5, width: DeviceWidth / 100 * 50, flexDirection: 'column', alignItems: 'flex-end', justifyContent: 'center', marginTop: 5 }}>
                        <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 12 }} >send to :</Text>
                    </View>
                    <View style={{ height: DeviceHeight / 100 * 5, width: DeviceWidth / 100 * 30, flexDirection: 'column', top: 3 }}>
                        <Dropdown
                            placeholder='Everyone'
                            placeholderTextColor={'#1492E6'}
                            label=''
                            dropdownPosition={0}
                            labelHeight={7}
                            style={{ fontSize: 15, fontFamily: 'Prompt-Regular' }}
                            affixTextStyle={{ fontFamily: 'Prompt-Regular' }}
                            labelTextStyle={{ fontFamily: 'Prompt-Regular' }}
                            titleTextStyle={{ fontFamily: 'Prompt-Regular' }}
                            itemTextStyle={{ fontSize: 10, fontFamily: 'Prompt-Regular' }}
                            pickerStyle={{ width: 100 }}
                            inputContainerStyle={{ marginLeft: 10, borderBottomColor: 'transparent' }}
                            data={code}
                            valueExtractor={({ value }) => value}

                        />
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <View style={{ flex: 1, flexDirection: 'column' }}>

                        {/* row1 */}
                        <View style={{ flexDirection: 'column', marginTop: 20, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 12, marginLeft: 65 }} >John Doe</Text>
                            </View>
                            <View style={{ flexDirection: 'row', height: DeviceHeight / 100 * 12, width: DeviceWidth }}>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20 }}>
                                    <Image source={Images.video_image} style={{ height: 36, width: 36, borderRadius: 18, position: 'absolute', left: 25, top: 30 }} />
                                </View>
                                <View style={{
                                    alignSelf: 'flex-start',
                                    flexDirection: 'column', backgroundColor: 'aliceblue', width: DeviceWidth / 100 * 60,
                                    borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomRightRadius: 15, borderBottomLeftRadius: 1, marginTop: 7
                                }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 12, margin: 15 }} >John Doe lorem ipsum dolor sit amet,
                                    sit amet lorem ipsum wow beautiful, dance
                                    by our school  </Text>
                                </View>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#2C2C2C', fontFamily: 'Prompt-Regular', fontSize: 12, marginRight: 25 }} >10:57</Text>
                                </View>
                            </View>
                        </View>

                        {/* row2 */}
                        <View style={{ flexDirection: 'column', marginTop: 20, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 12, marginLeft: 65 }} >Daniel Ribon</Text>
                            </View>
                            <View style={{ flexDirection: 'row', height: DeviceHeight / 100 * 12, width: DeviceWidth }}>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20 }}>
                                    <Image source={Images.video_image} style={{ height: 36, width: 36, borderRadius: 18, position: 'absolute', left: 25, top: 30 }} />
                                </View>
                                <View style={{
                                    alignSelf: 'flex-start', flexDirection: 'column', backgroundColor: 'aliceblue', width: DeviceWidth / 100 * 60,
                                    borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomRightRadius: 15, borderBottomLeftRadius: 1, marginTop: 7
                                }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 12, margin: 15 }} >lorem ipsum dolor sit amet
                                    thums up, to the whole team
                                    by our school  </Text>
                                </View>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#2C2C2C', fontFamily: 'Prompt-Regular', fontSize: 12, marginRight: 25 }} >10:57</Text>
                                </View>
                            </View>
                        </View>

                        {/* row3 */}
                        <View style={{ flexDirection: 'column', marginTop: 20, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row', height: DeviceHeight / 100 * 12, width: DeviceWidth }}>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 12, marginLeft: 45 }} >11:00</Text>
                                </View>
                                <View style={{
                                    alignSelf: 'flex-start', flexDirection: 'column', backgroundColor: 'springgreen', width: DeviceWidth / 100 * 60,
                                    borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomRightRadius: 1, borderBottomLeftRadius: 15, marginTop: 7, marginLeft: 3
                                }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 12, margin: 15 }} >lorem ipsum
                                    thums up, to the whole team
                                    by our school wow, beautiful our schoolmates  </Text>
                                </View>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20 }}>
                                    <Image source={Images.video_image} style={{ height: 36, width: 36, borderRadius: 18, position: 'absolute', right: 30, top: 30 }} />
                                </View>
                            </View>
                        </View>

                        {/* row4 */}
                        <View style={{ flexDirection: 'column', marginTop: 20, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 12, marginLeft: 65 }} >Daniel Ribon</Text>
                            </View>
                            <View style={{ flexDirection: 'row', height: DeviceHeight / 100 * 12, width: DeviceWidth }}>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20 }}>
                                    <Image source={Images.video_image} style={{ height: 36, width: 36, borderRadius: 18, position: 'absolute', left: 25, top: 30 }} />
                                </View>
                                <View style={{
                                    alignSelf: 'flex-start', flexDirection: 'column', backgroundColor: 'aliceblue', width: DeviceWidth / 100 * 60,
                                    borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomRightRadius: 15, borderBottomLeftRadius: 1, marginTop: 7
                                }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 12, margin: 15 }} >lorem ipsum dolor sit amet
                                    thums up, to the whole team
                                    by our school  </Text>
                                </View>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#2C2C2C', fontFamily: 'Prompt-Regular', fontSize: 12, marginRight: 25 }} >10:57</Text>
                                </View>
                            </View>
                        </View>

                        {/* row5 */}
                        <View style={{ flexDirection: 'column', marginTop: 20, marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 12, marginLeft: 65 }} >Daniel Ribon</Text>
                            </View>
                            <View style={{ flexDirection: 'row', height: DeviceHeight / 100 * 12, width: DeviceWidth }}>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20 }}>
                                    <Image source={Images.video_image} style={{ height: 36, width: 36, borderRadius: 18, position: 'absolute', left: 25, top: 30 }} />
                                </View>
                                <View style={{
                                    alignSelf: 'flex-start', flexDirection: 'column', backgroundColor: 'aliceblue', width: DeviceWidth / 100 * 60,
                                    borderTopLeftRadius: 15, borderTopRightRadius: 15, borderBottomRightRadius: 15, borderBottomLeftRadius: 1, marginTop: 7
                                }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Regular', fontSize: 12, margin: 15 }} >lorem ipsum dolor sit amet
                                    thums up, to the whole team
                                    by our school  </Text>
                                </View>
                                <View style={{ flexDirection: 'column', height: DeviceHeight / 100 * 10, width: DeviceWidth / 100 * 20, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#2C2C2C', fontFamily: 'Prompt-Regular', fontSize: 12, marginRight: 25 }} >10:57</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                    <TextInput
                        style={{
                            height: DeviceHeight / 100 * 7, width: DeviceWidth / 100 * 90, borderRadius: 5, borderWidth: 0.5, borderColor: 'lightblue',
                            backgroundColor: 'aliceblue', margin: 20
                        }}
                        placeholderTextColor={'black'}
                        returnKeyType='done'
                        keyboardType={'default'}
                    >
                    </TextInput>
                    <TouchableOpacity style={{ position: 'absolute', right: 60, top: 35 }}>
                        <Image source={Images.ismile1} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ position: 'absolute', right: 22, top: 30 }}>
                        <Image source={Images.isend1} style={{ color: 'green' }} />
                    </TouchableOpacity>
                </View>


                {/* Menu PopUp */}
                <BottomSheet
                    visible={this.state.visible}
                    onBackButtonPress={this._toggleBottomNavigationView}
                    onBackdropPress={this._toggleBottomNavigationView}
                >
                    <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{
                            borderRadius: 10, backgroundColor: 'white', width: '85%', height: '88%',
                            margin: 25
                        }}>
                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.camera_icon} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Select the Sound Device</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.toggle_camera} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Toggle Camera</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.enable_low_bandwidth} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Enable Low Bandwidth Mode</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.camera_icon} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Select the Sound Device</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.camera_icon} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Add Meeting Password</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.start_recording} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Start Recording</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.start_live_stream} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Start Live Stream</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.tile_view} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Enter Tile View</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.camera_icon} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Meeting Info</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.raise_hand} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Raise Your Hand</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.blur} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Blur Background</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                                <View style={{ flexDirection: 'column', height: 40, width: 90, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image source={Images.muteall_icon} />
                                </View>
                                <View style={{ flexDirection: 'column', height: 40, width: 280, alignItems: 'flex-start', justifyContent: 'center' }}>
                                    <Text style={{ color: '#131313', fontFamily: 'Prompt-Medium', fontSize: 14, marginRight: 25 }} >Muter All</Text>
                                </View>
                            </View>

                        </View>
                    </View>
                </BottomSheet>

            </View >
        )
    }
}





