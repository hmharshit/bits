import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity, StyleSheet, FlatList, AsyncStorage } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import { ToggleNotification } from "../../services/auth_services";
// import TopBar from '../../components/atoms/TopBar'
import Loader from "../../components/molecules/Loader"
import SwitchToggle from "react-native-switch-toggle";
import TopBar from '../../components/atoms/TobBar1'

export default class SettingScreen extends Component {
  constructor(props) {
    super(props)
    console.disableYellowBox = true

    this.state = {
      IsPushOn: false,
      IsLoader: false,
      refundSwitch: true
    }
  }

  componentDidMount() {
    this.fetchUserDetails()
  }


  toggleNotification = (enable) => {
    this.setState({ IsLoader: true })
    ToggleNotification(this.state.user_id, enable ? 1 : 0).then(response => {
      this.setState({ IsLoader: false })
      if (response && !response.error) {
        response = response.result

        if (response.message === "Updated successfully.") {
          this.setState({ refundSwitch: enable })
          AsyncStorage.setItem("user", JSON.stringify(response.user))
        }
      }
    })
  }

  fetchUserDetails = () => {
    AsyncStorage.getItem("user").then(user => {
      user = JSON.parse(user)

      this.setState({
        refundSwitch: user.notif_toggle === 1 ? true : false,
        user_id: user.id
      })
    })
  }

  render() {
    const { refundSwitch } = this.state

    return (
      <SafeAreaView style={styles.mainContainer}>
        <TopBar iconHide = {1} navigation={this.props.navigation}></TopBar>
        <View style={{ flex: 1, marginTop: 10 }}>
          <View style={styles.header}>
            <Text style={{ color: '#8BC341', fontSize: 14, fontFamily: 'Prompt-Regular' }}>SETTINGS</Text>
            {/* <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.pop()}>
              <Image source={Images.back_icon} />
            </TouchableOpacity> */}
          </View>

          <View style={[CommonStyles.ZView.contentView]}>
            <View style={{
              flex: 1, marginTop: 20, maxHeight: 50, borderBottomColor: Colors.UNDERLINE,
              borderBottomWidth: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row',
            }}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={styles.pushText}>Push Notifications</Text>
              </View>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                <SwitchToggle
                  containerStyle={{
                    width: 38,
                    height: 12,
                    borderRadius: 25,
                    padding: 1
                  }}
                  backgroundColorOn="lightgreen"
                  backgroundColorOff="lightgrey"
                  circleStyle={{
                    width: 18,
                    height: 18,
                    borderRadius: 9,
                  }}
                  switchOn={refundSwitch}
                  onPress={() => { this.toggleNotification(!this.state.refundSwitch) }} circleColorOff="grey"
                  circleColorOn="green"
                  duration={250}
                />
              </View>
              {/* <TouchableOpacity style={{ flex: 1, maxWidth: 80, justifyContent: 'center', alignItems: 'flex-end' }} onPress = {()=>{this.toggleNotification(!this.state.IsPushOn)}}>
                <Image source={ this.state.IsPushOn ? Images.toggle_on_icon : Images.toogle_off_icon} />
              </TouchableOpacity> */}
            </View>

            {/* <View style={{ flex: 1, marginTop: 15, maxHeight: 50, borderBottomColor: Colors.UNDERLINE, borderBottomWidth: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={styles.subscribe}>My Subscriptions</Text>
              </View>
              <TouchableOpacity style={{ flex: 1, maxWidth: 80, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Text style={styles.subscribe1}>Nil</Text>
              </TouchableOpacity>
            </View> */}

            {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Plan')} style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { marginLeft: 0, marginRight: 0, marginTop: 50 }]}>
              <Text style={styles.button}>Subscribe</Text>
            </TouchableOpacity> */}

            <View>

            </View>
          </View>
        </View>
        <Loader setModalVisible={this.state.IsLoader}></Loader>
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  contentView: {
    flex: 1,
    marginTop: 50
  },
  header: {
    flex: 1,
    maxHeight: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
  backIcon: {
    left: 20,
    width: 40,
    height: 40,
    position: 'absolute',
  },
  pushText: {
    color: '#131313', fontSize: 15, fontFamily: 'Prompt-Regular'
  },
  subscribe: {
    color: '#999999', fontSize: 15, fontFamily: 'Prompt-Regular'
  },
  button: {
    textAlign: 'center',
    color: '#FFFFFF', fontSize: 15, fontFamily: 'Prompt-Medium'

  },
  subscribe1: {
    color: '#131313', fontSize: 15, fontFamily: 'Prompt-Regular'
  },
});
