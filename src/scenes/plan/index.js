import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import Loader from "../../components/molecules/Loader"
import TopBar from '../../components/atoms/TobBar1'
import { FetchPlansList } from "../../services/plan_services"
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';


// const routes = [
//     { key: 'premium', title: 'Premium', price: '9.99', host: 'Per Host/Month', participent: '20 Participants', time: '1', space: '2 GB', trial: '5' },
//     { key: 'basic', title: 'Basic', price: '9.99', host: 'Per Host/Month', participent: '20 Participants', time: '1', space: '2 GB', trial: '5' },
//     { key: 'standard', title: 'Standard', price: '9.99', host: 'Per Host/Month', participent: '20 Participants', time: '1', space: '2 GB', trial: '5' },
//     { key: 'premium', title: 'Premium', price: '9.99', host: 'Per Host/Month', participent: '20 Participants', time: '1', space: '2 GB', trial: '5' },
// ];
const initialLayout = { width: Dimensions.get('window').width };



export default class PlanScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
            index: 0,
            isLoader: false,
            plans_list: []
        }

    }


    _fetchPlans() {
        this.setState({ isLoader: true })

        FetchPlansList().then(response => {
            let plans = []
            if (response && !response.error) {
                response = response.result
                if (response.message === "Plan list.") {
                    response.meeting.forEach(plan => {
                        let _plan = {
                            key: plan.title.toLowerCase(),
                            title: plan.title,
                            price: plan.price,
                            host: 'Per Host/Month',
                            participent: `${plan.participants} Participants`,
                            time: `${plan.call_duration}`,
                            space: `${plan.recording} GB`,
                            trial: '5',
                            id: plan.id
                        }

                        plans.push(_plan)
                    });
                }
            }
            this.setState({ isLoader: false, plans_list: plans })
        })
    }


    componentDidMount() {
        this._fetchPlans()
    }



    renderLabel = ({ value, image }) => {
        return (
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center' }} >
                    <Image source={image} />
                </View>
                <View style={{ width: 180, height: 50, justifyContent: 'center' }} >
                    <Text style={{ color: 'grey' }}>{value}</Text>
                </View>
            </View>

        )
    };


    renderTabBar = (props) => (
        <TabBar
            {...props}
            activeColor={'#131313'}
            inactiveColor={'#999999'}
            renderLabel={({ route, focused, color }) => (
                <Text style={{ color }}>
                    {route.title}
                </Text>
            )}
            indicatorStyle={{ backgroundColor: 'white' }}
            style={{ backgroundColor: 'white' }}
        />
    );


    renderScene = ({ route }) => {
        return (
            <View style={{ flex: 1, width: '100%', height: '100%' }}>

                <View style={{ margin: 40, marginLeft: 60, marginRight: 60, flex: 1, justifyContent: 'center', alignContent: 'center', borderColor: Colors.PRIMARY, borderWidth: 1, borderRadius: 15 }}>


                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                        <View style={{ minHeight: 100, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                            <View style={{ height: 50, marginLeft: 5, marginRight: 5 }} >
                                <Text style={{ fontFamily: 'Prompt-Regular', fontSize: 24, color: '#8BC341' }}>$</Text>
                            </View>
                            <View style={{ height: 50, marginTop: 10 }} >
                                <Text style={{ fontFamily: 'Prompt-Bold', fontSize: 39, color: '#8BC341' }}>{route.price}</Text>
                            </View>
                            <View style={{ width: 60, height: 50, justifyContent: 'center', alignItems: 'center' }} >
                                <Text style={{ fontFamily: 'Prompt-Regular', fontSize: 24, color: '#8BC341' }}>{"/mo"}</Text>

                            </View>
                        </View>
                        <View style={{ flex: 1, minHeight: 100 }} >
                            {this.renderLabel({ value: route.host, image: Images.pricing_grey })}
                            {this.renderLabel({ value: route.participent, image: Images.pricing_participant })}
                            {this.renderLabel({ value: route.time + " hour continous", image: Images.pricing_time })}
                            {this.renderLabel({ value: route.space + " GB Recording Space", image: Images.pricing_space })}
                        </View>
                        <View style={{ minHeight: 100, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ fontFamily: 'Prompt-Regular', fontSize: 15, color: '#8BC341' }}>{route.trial + " Meeting Free Trial"}</Text>
                        </View>
                    </View>
                </View>
            </View>);
    };


    render() {

        const { plans_list } = this.state
        console.log(plans_list)
        return (
            <SafeAreaView style={styles.mainContainer}>
                <TopBar navigation={this.props.navigation}></TopBar>
                <View style={{ flex: 1, marginTop: 10 }}>

                    <View style={styles.header}>
                        <Text style={{ color: Colors.PRIMARY, fontSize: Typography.FONT_SIZE_14 }}>PLANS</Text>
                        {/* <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigation.pop()}>
                            <Image source={Images.back_icon} />
                        </TouchableOpacity> */}
                    </View>

                    <View style={[CommonStyles.ZView.contentView, { marginLeft: 0, marginRight: 0 }]}>

                        <TabView
                            renderTabBar={this.renderTabBar}
                            navigationState={{ index: this.state.index, routes: plans_list }}
                            renderScene={this.renderScene}
                            onIndexChange={(indexVal) => { this.setState({ index: indexVal }) }}
                            initialLayout={initialLayout}
                        />

                        <View style={{ flex: 1, maxHeight: 80 }}>
                            <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { marginLeft: 50, marginRight: 50, }]} onPress={() => { this.props.navigation.navigate('Payment') }}>
                                <Text style={{ color: '#FFFFFF', fontSize: 15, fontFamily: 'Prompt-Medium' }}>Subscribe</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <Loader setModalVisible={this.state.isLoader}></Loader>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentView: {
        flex: 1,
        marginTop: 50
    },
    header: {
        flex: 1,
        maxHeight: 64,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backIcon: {
        left: 20,
        width: 40,
        height: 40,
        position: 'absolute',
    }
});
