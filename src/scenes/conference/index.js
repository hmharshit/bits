import React, { useEffect } from "react"
import { View, BackHandler } from "react-native"
import JitsiMeet, { JitsiMeetView } from "react-native-jitsi-meet"
import { connect } from "react-redux"


const VideoCall = (props) => {




    useEffect(() => {

      BackHandler.addEventListener('hardwareBackPress', () => true)

      let userInfo, url

      if (props.navigation.state.params && props.navigation.state.params.url) {
        userInfo = props.navigation.state.params.userInfo || {}
        url = props.navigation.state.params.url
      }
      setTimeout(() => {
        console.log(url, userInfo)
        JitsiMeet.call(`${url}#config.startWithAudioMuted=true&config.startWithVideoMuted=true`, userInfo);
      }, 100);
      
      return () => {
        BackHandler.removeEventListener('hardwareBackPress')
        JitsiMeet.endCall();
      };
    
    })
  
    function onConferenceTerminated(nativeEvent) {
      /* Conference terminated event */
      BackHandler.removeEventListener('hardwareBackPress')


      console.log("terminationg")
      JitsiMeet.endCall();
      // props.navigation.pop()
      props.navigation.navigate(props.isUserLoggedIn ? 'Home' : 'Auth')
      // console.log("@@@@@@@@@@@@@@@@", nativeEvent)
    }
  
    function onConferenceJoined(nativeEvent) {
      /* Conference joined event */
      console.log("!!!!!!!!1", "joined")

    }
  
    function onConferenceWillJoin(nativeEvent) {
      /* Conference will join event */
      console.log("################", "will join")

    }

      return (
        <View style={{ backgroundColor: 'white',flex: 1 }}>
          <JitsiMeetView 
            onConferenceTerminated={e => onConferenceTerminated(e)}
            onConferenceJoined={e => onConferenceJoined(e)}
            onConferenceWillJoin={e => onConferenceWillJoin(e)} 
            style={{ flex: 1, height: '100%', width: '100%' }} />
        </View>
      )
  }

  const _mapStateToProps = (state) => {
    return {
      isUserLoggedIn: state['home'].is_user_loggedin
    }
  }
  export default connect(_mapStateToProps)(VideoCall);