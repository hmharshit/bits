import React, { Component, useEffect } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { Images, CommonStyles } from "../../styles";
import SigninCard from "../../components/organisms/SigninCard";
import Loader from "../../components/molecules/Loader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { ForgotPassword } from "../../services/auth_services"
import { connect } from "react-redux"
import { Notification } from "../../components/util"
import { setUserId } from "../../actions/login"

const { width: DeviceWidth, height: DeviceHeight } = Dimensions.get('window')

class ForgotPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      password: "",
      IsLoader: false,
      country_code: "91"
    };
  }



  checkValidNumber(number) {
    if (number.indexOf(".") == -1 && number.length == 10) return true
    else return false
  }



  sendOtp = () => {

    if (!this.state.phone) {
      Notification("Please enter the mobile number!", "warning", 0)
      return false
    }

    if (!this.checkValidNumber(this.state.phone)) {
      Notification("Please enter a valid number!", "warning", 0)
      return false
    }

    this.setState({ IsLoader: true });
    ForgotPassword(this.state.phone, this.state.country_code).then(response => {
      console.log("Response --> ", response)
      this.setState({ IsLoader: false });

      if (response && response.error == false) {
        let result = response.result;

        if (result && result.message && result.message == "OTP send successfully") {
          Notification("OTP sent successfully!", "success", 0)
          console.log("user", result.user_id)
          this.props.dispatch(setUserId(result.user_id))
          this.setState({ otp: result.otp.toString(), user_id: result.user_id, phone_number_attached: this.state.phone })
        }
      }

      else {
        Notification(response.result.message, "warning", 0)
      }

    }).catch(err => {
      this.setState({ IsLoader: false });
      console.log(err)
    })
  }

  onStateUpdated(data) {
    const { phone, password } = { data };
    this.setState({ phone: data.phone, password: data.password });
    console.log(this.state);
  }

  resetPassword = () => {
    const { phone, otp, user_id, password } = this.state

    console.log(phone, otp, user_id, password)
    if (!phone || phone.length != 10) {
      Notification("Please enter the mobile number!", "warning", 0)
      return false
    }

    if (!otp || !user_id || !password) {
      Notification("Please enter the OTP!", "warning", 0)
      return false
    }

    if (otp !== password) {
      Notification("Wrong OTP!", "error", 0)
      return false
    }

    if (phone != this.state.phone_number_attached) {
      Notification("This OTP is not valid for this number.", "error", 0)
      return false
    }


    Notification("OTP verified!", "success", 0)
    this.props.navigation.navigate('NewPassword')


  }


  render() {
    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <KeyboardAwareScrollView
          style={{ flex: 1, width: "100%" }}
          contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
        >
          <View style={CommonStyles.ZView.container}>
            <View
              style={[
                CommonStyles.ZView.container,
                CommonStyles.ZView.logoNewView,
              ]}
            >
              <TouchableOpacity style={{ marginLeft: 20, marginRight: 10, marginTop: 10 }}
                onPress={() => this.props.navigation.pop()}
              >
                <Image source={Images.back_image} />
              </TouchableOpacity>

              <Image source={Images.logo} style={{
                justifyContent: 'center', alignItems: 'center', position: 'absolute',
                left: DeviceWidth / 100 * 26, top: DeviceHeight / 100 * 10
              }} />
            </View>

            <View style={[CommonStyles.ZView.centerView2]}>
              <SigninCard
                country_code_handler={(cc) => this.setState({ country_code: cc })}
                showVerify={true}
                verifyButtonHandler={() => this.sendOtp()}
                resendOtp={() => this.sendOtp()}
                cardType={"forgot_password"}
                dataChanged={(data) => {
                  this.onStateUpdated(data);
                }}
              />
            </View>

            <View
              style={[
                CommonStyles.ZView.container,
                { justifyContent: "space-evenly", bottom: 30 },
              ]}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "center",
                }}
              >
                <TouchableOpacity
                  style={[
                    CommonStyles.ZButton.fullgreen,
                    CommonStyles.ZView.centerImageView,
                  ]}
                  onPress={() => {
                    this.resetPassword();
                  }}
                >
                  <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Reset Password</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Loader setModalVisible={this.state.IsLoader}></Loader>
      </SafeAreaView>
    );
  }
}

const _mapStateToProps = (state) => {
  return {

  }
}
export default connect(_mapStateToProps)(ForgotPasswordScreen);
