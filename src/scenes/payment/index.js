import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import TopBar from '../../components/atoms/TopBar2'

export default class PaymentScreen extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <SafeAreaView style={styles.mainContainer}>
                <TopBar navigation={this.props.navigation}></TopBar>
                <View style={{ flex: 1, marginTop: 10 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={Images.payment_success_icon} />
                        <Text style={{ marginTop: 20, marginBottom: 15, fontFamily: 'Prompt-Regular', fontSize: 28, color: '#131313' }}>Wow!</Text>
                        <Text style={{ fontFamily: 'Prompt-Regular', fontSize: 18, color: '#999999' }}>Payment Success</Text>
                    </View>

                    <View style={{ flex: 1, maxHeight: 100 }}>
                        <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView]} onPress={() => { this.props.navigation.navigate('Home') }}>
                            <Text style={{ fontFamily: 'Prompt-Medium', fontSize: 15, color: '#FFFFFF' }}>Continue</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentView: {
        flex: 1,
        marginTop: 50
    },
    header: {
        flex: 1,
        maxHeight: 64,
        alignItems: 'center',
        justifyContent: 'center',
    },
    backIcon: {
        left: 20,
        width: 40,
        height: 40,
        position: 'absolute',
    }
});
