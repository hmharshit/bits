import React, { Component } from 'react';
import { SafeAreaView, View, Image, Text, TouchableOpacity, AsyncStorage, StyleSheet, BackHandler } from 'react-native';
import Loader from '../../components/molecules/Loader'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from "react-redux"
import { RegisterUser } from "../../services/auth_services"
import { Images, CommonStyles, Colors, Typography } from '../../styles'
import ZingTextInput from '../../components/molecules/ZingTextInput'
import { Dropdown } from 'react-native-material-dropdown'
import { UpdatePassword } from "../../services/auth_services"
import { Notification } from "../../components/util"
import { setIsUserLoggedIn, setUserData } from "../../actions/home"


class NewPasswordScreen extends Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true

    this.state = {
      phone: '',
      password: '',
      IsLoader: false,
      timezones: [],
    }

    this.onSigninDidSelected = this.onSigninDidSelected.bind(this)
  }

  onSigninDidSelected = () => {

    let { password, confirm_password } = this.state

    console.log(password, confirm_password, this.props._userId)

    if (!password || !confirm_password || !this.props._userId) {
      Notification("All fields are mandatory!", "warning", 0)
      return false
    }

    if(!password.trim() || !confirm_password.trim()) {
      Notification("Please enter correct values", "warning", 0);
      return false;
    }

    if (!this.checkPassword()) {
      Notification("Passwords don't match!", "error", 0)
      return false
    }

    this.setState({ IsLoader: true })

    UpdatePassword(this.props._userId, password).then(resp => {
      this.setState({ IsLoader: false });
      if (resp && !resp.error) {
        Notification("Your password has been updated!", "success", 0)
        this.props.navigation.navigate('Login')
      }
      else {
        Notification(resp.result.message, "error", 0)
      }
    }).catch(err => {
      this.setState({ IsLoader: false });
      console.log(err)})
  }

  checkPassword() {
    const {
      password,
      confirm_password
    } = this.state

    if (password === confirm_password) return true
    else return false
  }


  render() {

    return (
      <SafeAreaView style={CommonStyles.ZView.container}>
        <KeyboardAwareScrollView style={{ flex: 1, width: '100%' }} contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
          <View style={[CommonStyles.ZView.container]}>
            <TouchableOpacity style={{ marginLeft: 20, marginRight: 10, marginTop: 20 }}
                onPress={() => this.props.navigation.pop()}
              >
                <Image source={Images.back_image} />
              </TouchableOpacity>
            <View style={[CommonStyles.ZView.container, CommonStyles.ZView.logoView]}>
              <Image source={Images.logo} style={{ top: 25 }} />
            </View>

            <View style={[CommonStyles.ZView.centerView, { justifyContent: 'center', alignItems: 'center', flex: 1 }]}>
              <View style={{
                 height: 80, alignSelf: 'center',
                justifyContent: 'center', alignItems: 'center'

              }}>
                <Text style = {{ marginLeft:10, marginRight:10, textAlign: 'center', color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15}}>{"We have successfully verified your mobile \n Please enter the details to reset your password"}</Text>
                {/* <Text style={{  textAlign: 'center', color: '#999999', fontFamily: 'Prompt-Regular', fontSize: 15 }} multiline={true}>{"We have successfully verified your mobile \n Please enter the details to reset your password"}</Text> */}
              </View>
              <ZingTextInput extraStyle={{ marginBottom: 10, marginTop: 15, fontFamily: 'Prompt-Regular', fontSize: 15, color: '#131313' }} phoneOptions={false} secureTextEntry={false} placeholder={"New Password"} onChangeText={(text) => { this.setState({ password: text }) }} ></ZingTextInput>
              <ZingTextInput extraStyle={{ fontFamily: 'Prompt-Regular', fontSize: 15, color: '#131313' }} phoneOptions={false} secureTextEntry={false} placeholder={"Confirm Password"} onChangeText={(text) => { this.setState({ confirm_password: text }) }} ></ZingTextInput>

              <TouchableOpacity style={[CommonStyles.ZButton.fullgreen, CommonStyles.ZView.centerImageView, { margin: 80, marginTop:100 }]} onPress={this.onSigninDidSelected}>
                <Text style={{ color: '#FFFFFF', fontFamily: 'Prompt-Medium', fontSize: 15 }}>Reset Password</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Loader setModalVisible={this.state.IsLoader} ></Loader>

      </SafeAreaView>
    )
  }
}

const _mapStateToProps = (state) => {
  console.log(state["login"])
  return {
    _userId: state["login"].user_id
  }
}

export default connect(_mapStateToProps)(NewPasswordScreen);


export const passwordStyles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    textAlign: 'center',
    width: 330,
    height: 100,
    marginBottom: 10,
    lineHeight: 25,
    color: Colors.TEXT_PLACEHOLDER,
    fontSize: Typography.FONT_SIZE_16
  },
});                                                                                                                                                           