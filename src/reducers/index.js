import { combineReducers } from 'redux';
import loginReducer  from "./login"
import registerReducer from "./register"
import homeReducer from "./home"
 
// Combine all the reducers
const rootReducer = combineReducers({
    login: loginReducer,
    register: registerReducer,
    home: homeReducer
})
 
export default rootReducer;