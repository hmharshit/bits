import {
    SET_PHONE_LOGIN_VISIBLE,
    SET_USER_ID
} from "../actions/types"

const loginReducer = (state={
    user_id: null
}, action) => {
    switch(action.type) {
        case SET_PHONE_LOGIN_VISIBLE:
            return {
                ...state,
                phoneLoginVisible: action.visible
            }

        case SET_USER_ID:
            return {
                ...state,
                user_id: action.user_id
            }
        default:
            return state;
    }
}

export default loginReducer;