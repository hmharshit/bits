import {
    SET_ENABLE_OTP_FIELD,
    SET_REGISTRATION_MOBILE,
    SET_COUNTRY_CODE
} from "../actions/types"


const initialState = {
    otp_field_enabled: false
}

const registerReducer = (state=initialState, action) => {
    switch(action.type) {
        case SET_ENABLE_OTP_FIELD:
            return {
                ...state,
                otp_field_enabled: action.enable
            }
        case SET_REGISTRATION_MOBILE:
            return {
                ...state,
                mobile_number: action.mobile_number
            }
        case SET_COUNTRY_CODE:
            return {
                ...state,
                country_code: action.country_code
            }
        default:
            return state;
    }
}

export default registerReducer;