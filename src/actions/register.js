import {
    SET_ENABLE_OTP_FIELD,
    SET_REGISTRATION_MOBILE,
    SET_COUNTRY_CODE
} from "./types"


export function setEnableOtpfield(enable) {
    return {
        type: SET_ENABLE_OTP_FIELD,
        enable
    };
}


export function setRegistrationMobile(mobile_number) {
    return {
        type: SET_REGISTRATION_MOBILE,
        mobile_number
    };
}


export function setCountryCode(country_code) {
    return {
        type: SET_COUNTRY_CODE,
        country_code
    };
}