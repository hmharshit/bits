import {
    SET_IS_USER_LOGGED_IN,
    SET_USER_META_DATA,
    SET_USER_NAVIGATED_FROM_INITIAL_URL
} from "./types"


export function setUserData(user_data) {
    return {
        type: SET_USER_META_DATA,
        user_data
    };
}


export function setIsUserLoggedIn(user_logged_in) {
    return {
        type: SET_IS_USER_LOGGED_IN,
        user_logged_in
    };
}


export function setUserNavigatedFromUrl(navigated) {
    return {
        type: SET_USER_NAVIGATED_FROM_INITIAL_URL,
        navigated
    };
}

