import React, { useEffect, useState } from "react";
import { Provider } from "react-redux";
import store from "./store";
import SplashScreen from "react-native-splash-screen";
import Navigator from "./navigations";
import { Root } from "native-base";
import AppOfflineModal from "./components/organisms/AppOfflineModal";

const App = () => {
  const [isOffline, setOffline] = useState(false);
  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  const checkPing = () => {
    return fetch("https://google.com")
      .then((resp) => resp)
      .then((resp) => {
        setOffline(false);
        SplashScreen.hide();
      })
      .catch((error) => {
        if (
          error.message === "Timeout" ||
          error.message === "Network request failed"
        ) {
          setOffline(true);
        }
      });
  };

  useEffect(() => {
    async function handler() {
      delay(2500);
      checkPing();
    }
    handler();
  });

  return isOffline ? (
    <AppOfflineModal checkInternet={() => checkPing()} open={isOffline} />
  ) : (
    <Provider store={store}>
      <Root>
        <Navigator />
      </Root>
    </Provider>
  );
};

export default App;
